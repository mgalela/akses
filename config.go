package hard

import (
	"encoding/json"
	"io/ioutil"

	log "github.com/Sirupsen/logrus"
)

type configuration struct {
	MemoryThreshold     uint64 `json:"MemoryThreshold"`
	CPUThreshold        uint64 `json:"CPUThreshold"`
	MailgunDomain       string `json:"MailgunDomain"`
	MailgunPrivateKey   string `json:"MailgunPrivateKey"`
	MailgunPublicKey    string `json:"MailgunPublicKey"`
	LogLevel            string `json:"LogLevel"`
	AdsServer           string `json:"AdsServer"`
	AdsKey              string `json:"AdsKey"`
	ConfirmBaseUri      string `json:"ConfirmBaseUri"`
	ForgotPasswdBaseUri string `json:"ForgotPasswdBaseUri"`
	BoksServer          string `json:"BoksServer"`
}

const (
	testModeIP = "114.79.59.52"
)

var (
	Config   = &configuration{}
	testMode = false
)

// Initialize will initialize a configuration from a json file.
// It receive a string of the configuration file name.
func Initialize(filename string) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal("Can't read configuration file:", err)
		return err
	}

	err = json.Unmarshal(data, Config)
	if err != nil {
		log.Fatal("Can't read configuration file:", err)
		return err
	}

	/*if len(Config.GcmKey) == 0 {
		log.Fatal("Please specify gcm key")
		return errGcmKeyEmpty
	}*/

	return nil
}
