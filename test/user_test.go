package apptest

import (
	"testing"

	"github.com/kataras/iris"
	"gopkg.in/mgo.v2/bson"

	"github.com/mgalela/akses/access"
	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"

	. "github.com/smartystreets/goconvey/convey"
)

const (
	defaultPass = "passworddefault"
)

var (
	tt = []access.UserCreateRequest{
		{
			Source:    "local",
			Username:  "adminuser",
			Email:     "admin@test.com",
			Password:  defaultPass,
			FirstName: "admin",
			LastName:  "test",
			Role:      1,
			Gcm:       "1234567890",
			Apn:       "0987654321",
			Active:    1,
			RegBy:     "system",
		},
		{
			Source:    "local",
			Username:  "operator",
			Email:     "opr@test.com",
			Password:  defaultPass,
			FirstName: "opr",
			LastName:  "test",
			Role:      2,
			Gcm:       "2234567890",
			Apn:       "0987654322",
			Active:    1,
			RegBy:     "system",
		},
		{
			Source:    "local",
			Username:  "user001",
			Email:     "user1@test.com",
			Password:  defaultPass,
			FirstName: "user1",
			LastName:  "test",
			Role:      3,
			Gcm:       "3234567890",
			Apn:       "0987654323",
			Active:    1,
			RegBy:     "",
		},
		{
			Source:    "local",
			Username:  "user002",
			Email:     "user2@test.com",
			Password:  defaultPass,
			FirstName: "user2",
			LastName:  "test",
			Role:      3,
			Gcm:       "4234567890",
			Apn:       "0987654324",
			Active:    2,
			RegBy:     "",
		},
	}
)

func TestUserCreate(t *testing.T) {
	Convey("Test User Create", t, func() {
		tt1 := []access.UserCreateRequest{
			{
				Source:    "local",
				Username:  "adminuser",
				Email:     "admin1@test.com",
				Password:  defaultPass,
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				RegBy:     "system",
			},
			{
				Source:    "local",
				Username:  "adminuser1",
				Email:     "admin@test.com",
				Password:  defaultPass,
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				RegBy:     "system",
			},
			{
				Source:    "local",
				Username:  "",
				Email:     "", //no email
				Password:  defaultPass,
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				RegBy:     "system",
			},
			{
				Source:    "local",
				Username:  "",
				Email:     "admin1@gmail.com",
				Password:  "", //no password
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				RegBy:     "system",
			},
			{
				Source:    "local",
				Username:  "admin", //invalid username: character less than 8
				Email:     "admin1@gmail.com",
				Password:  defaultPass,
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				RegBy:     "system",
			},
			{
				Source:    "local",
				Username:  "adm|nuser", //invalid username
				Email:     "admin1@gmail.com",
				Password:  defaultPass,
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				RegBy:     "system",
			},
			{
				Source:    "local",
				Username:  "",
				Email:     "admin1gmail.com", //invalid email
				Password:  defaultPass,
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				RegBy:     "system",
			},
			{
				Source:    "local",
				Username:  "",
				Email:     "admin1@gmail.com",
				Password:  "1234567", //no less than 8
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				RegBy:     "system",
			},
		}
		utils.InitLog()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}

		Convey("Succeed", func() {
			db.Users(dbses).DropCollection()
			for _, v := range tt {
				regreq := v
				p := access.CreateUser{
					Dbsess: dbsess,
					Req:    regreq,
				}
				u := new(access.User)
				uc, err := u.Create(p)
				if err != nil {
					t.Fatalf("failed create user: %v", err)
				}
				ucheck := uc.(*access.User)
				u, err = access.UserByID(dbses, ucheck.ID, bson.M{})
				So(u.Email, ShouldEqual, v.Email)
				So(u.Username, ShouldEqual, v.Username)
				So(u.FirstName, ShouldEqual, v.FirstName)
				So(u.LastName, ShouldEqual, v.LastName)
				So(u.Gcm, ShouldContain, v.Gcm)
				So(u.Apn, ShouldContain, v.Apn)
				if v.RegBy == "system" {
					So(u.Active, ShouldEqual, v.Active)
					So(u.Role, ShouldEqual, v.Role)
				} else {
					So(u.Role, ShouldEqual, 3)
					So(u.Active, ShouldEqual, 1)
				}
			}
		})

		Convey("Failed: username exist", func() {
			regreq := tt1[0]
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.User)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "username had been taken")
		})

		Convey("Failed: email exist", func() {
			regreq := tt1[1]
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.User)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "email had been taken")
		})

		Convey("Failed: no email", func() {
			regreq := tt1[2]
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.User)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "empty email")
		})

		Convey("Success: no password", func() {
			regreq := tt1[3]
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.User)
			uc, err := u.Create(p)
			So(err, ShouldBeNil)
			if err != nil {
				t.Fatalf("failed create user: %v", err)
			}
			u = uc.(*access.User)
			ucheck, err := access.UserByID(dbses, u.ID, bson.M{})
			So(ucheck.Username, ShouldEqual, regreq.Username)
		})

		Convey("Failed: invalid username character length", func() {
			regreq := tt1[4]
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.User)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "username length must be between 6 and 40 characters")
			//db.Users(dbses).DropCollection()
		})

		Convey("Failed: invalid username", func() {
			regreq := tt1[5]
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.User)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "username format invalid")
			//db.Users(dbses).DropCollection()
		})

		Convey("Failed: invalid email format", func() {
			regreq := tt1[6]
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.User)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "email format invalid")
		})

		Convey("Failed: password less than 7 char", func() {
			regreq := tt1[7]
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.User)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "password must be more than 7 characters")
			db.Users(dbses).DropCollection()
		})

		Reset(func() {
			dbses.Close()
		})
	})
}

func TestUserDelete(t *testing.T) {
	Convey("Test User Delete", t, func() {
		utils.InitLog()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}
		u := new(access.User)

		db.Users(dbses).DropCollection()
		i := []string{}
		for _, v := range tt {
			regreq := v
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}

			uc, err := u.Create(p)
			if err != nil {
				t.Fatalf("failed create user: %v", err)
			}
			u = uc.(*access.User)
			i = append(i, u.ID)
			ucheck, err := access.UserByID(dbses, u.ID, bson.M{})
			So(ucheck.Username, ShouldEqual, v.Username)
		}
		Convey("Delete success", func() {
			res, err := u.Delete(dbsess, i[1])
			So(err, ShouldBeNil)
			So(res, ShouldEqual, 0)
			_, err = access.UserByID(dbses, i[1], bson.M{})
			So(res, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "user not found")
		})
		Convey("Failed: ID not found", func() {
			res, err := u.Delete(dbsess, "1")
			So(err, ShouldNotBeNil)
			So(res, ShouldEqual, iris.StatusNotFound)
		})

		Reset(func() {
			db.Users(dbses).DropCollection()
			dbses.Close()
		})

	})
}

func TestUserUpdate(t *testing.T) {
	Convey("Test User Update", t, func() {
		ttupdate := []access.UserUpdateReq{
			{
				ID:        "",
				Username:  "admin", //invalid username: character less than 6
				Email:     "admin1@gmail.com",
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				Active:    1,
			},
			{
				ID:        "",
				Username:  "adm|nuser", //invalid username
				Email:     "admin1@gmail.com",
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				Active:    1,
			},
			{
				ID:        "",
				Username:  "",
				Email:     "admin1gmail.com", //invalid email
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				Active:    1,
			},
			{
				ID:        "",
				Username:  "operator", //username exist
				Email:     "admin@test.com",
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				Active:    1,
			},
			{
				ID:        "",
				Username:  "adminuser",
				Email:     "opr@test.com", //email exist
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				Active:    1,
			},
			{
				ID:        "",
				Username:  "",
				Email:     "admin@gmail.com", //change email
				FirstName: "admingmail",      //change firstname
				LastName:  "testgmail",       //change lastname
				Role:      2,                 //change role
				Gcm:       "1234567891",      //change gcm
				Apn:       "0987654322",      //change apn
				Active:    2,                 //change active
			},
			{
				ID:        "",
				Username:  "adminuser1", //change username
				Email:     "admin@test.com",
				FirstName: "admintest",  //change firstname
				LastName:  "testtest",   //change lastname
				Role:      3,            //change role
				Gcm:       "1234567892", //change gcm
				Apn:       "0987654323", //change apn
				Active:    3,            //change active
			},
		}

		utils.InitLog()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}
		u := new(access.User)

		db.Users(dbses).DropCollection()
		i := []string{}
		for _, v := range tt {
			regreq := v
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}

			uc, err := u.Create(p)
			if err != nil {
				t.Fatalf("failed create user: %v", err)
			}
			u = uc.(*access.User)
			i = append(i, u.ID)
			ucheck, err := access.UserByID(dbses, u.ID, bson.M{})
			So(ucheck.Username, ShouldEqual, v.Username)
		}
		//t.Logf("i: %v", i[0])
		Convey("failed: username less than 6 char", func() {
			ut := new(access.User)
			updt := access.UpdateUser{
				Dbsess: dbsess,
				Req:    ttupdate[0],
			}
			err := ut.Update(updt, i[0])
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "username length must be between 6 and 40 characters")
		})
		Convey("failed: invalid username", func() {
			ut := new(access.User)
			updt := access.UpdateUser{
				Dbsess: dbsess,
				Req:    ttupdate[1],
			}
			err := ut.Update(updt, i[0])
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "username format invalid")
		})
		Convey("failed: invalid email", func() {
			ut := new(access.User)
			updt := access.UpdateUser{
				Dbsess: dbsess,
				Req:    ttupdate[2],
			}
			err := ut.Update(updt, i[0])
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "email format invalid")
		})
		Convey("failed: other username exist", func() {
			ut := new(access.User)
			updt := access.UpdateUser{
				Dbsess: dbsess,
				Req:    ttupdate[3],
			}
			err := ut.Update(updt, i[0])
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "username had been taken")
		})
		Convey("failed: other email exist", func() {
			ut := new(access.User)
			updt := access.UpdateUser{
				Dbsess: dbsess,
				Req:    ttupdate[4],
			}
			err := ut.Update(updt, i[0])
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "email had been taken")
		})
		Convey("success: change email and the rest", func() {
			ut := new(access.User)
			updt := access.UpdateUser{
				Dbsess: dbsess,
				Req:    ttupdate[5],
			}
			err := ut.Update(updt, i[0])
			So(err, ShouldBeNil)
			if err != nil {
				t.Fatalf("failed update user: %v", err)
			}
			//uc := res.(*access.User)
			uc, err := access.UserByID(dbses, i[0], bson.M{})
			So(uc.Email, ShouldEqual, ttupdate[5].Email)
			So(uc.Username, ShouldEqual, tt[0].Username)
			So(uc.FirstName, ShouldEqual, ttupdate[5].FirstName)
			So(uc.LastName, ShouldEqual, ttupdate[5].LastName)
			So(uc.Role, ShouldEqual, ttupdate[5].Role)
			So(uc.Gcm, ShouldContain, ttupdate[5].Gcm)
			So(uc.Apn, ShouldContain, ttupdate[5].Apn)
			So(uc.Active, ShouldEqual, ttupdate[5].Active)
		})
		Convey("success: change username and the rest", func() {
			ut := new(access.User)
			updt := access.UpdateUser{
				Dbsess: dbsess,
				Req:    ttupdate[6],
			}
			err := ut.Update(updt, i[0])
			So(err, ShouldBeNil)
			if err != nil {
				t.Fatalf("failed update user: %v", err)
			}
			//u := res.(*access.User)
			u, err := access.UserByID(dbses, i[0], bson.M{})
			So(u.Email, ShouldEqual, tt[0].Email)
			So(u.Username, ShouldEqual, ttupdate[6].Username)
			So(u.FirstName, ShouldEqual, ttupdate[6].FirstName)
			So(u.LastName, ShouldEqual, ttupdate[6].LastName)
			So(u.Role, ShouldEqual, ttupdate[6].Role)
			So(u.Gcm, ShouldContain, ttupdate[6].Gcm)
			So(u.Apn, ShouldContain, ttupdate[6].Apn)
			So(u.Active, ShouldEqual, ttupdate[6].Active)
			db.Users(dbses).DropCollection()
		})

		Reset(func() {
			db.Users(dbses).DropCollection()
			dbses.Close()
		})

	})
}

/*
  1. Read single user
  2. Read multiple users, with params:
	 - single id
	 - multiple ids
	 - email
	 - username
	 - role
	 - active
	 - confirm
	 - regby
	 - page
*/
func TestUserRead(t *testing.T) {
	Convey("Test user read", t, func() {
		tt2 := []access.UserCreateRequest{
			{
				Source:    "local",
				Username:  "adminuser",
				Email:     "admin@test.com",
				Password:  defaultPass,
				FirstName: "admin",
				LastName:  "test",
				Role:      1,
				Gcm:       "1234567890",
				Apn:       "0987654321",
				Active:    1,
				RegBy:     "system",
			},
			{
				Source:    "local",
				Username:  "operator",
				Email:     "opr@test.com",
				Password:  defaultPass,
				FirstName: "opr",
				LastName:  "test",
				Role:      2,
				Gcm:       "2234567890",
				Apn:       "0987654322",
				Active:    3,
				RegBy:     "system",
			},
			{
				Source:    "local",
				Username:  "user001",
				Email:     "user1@test.com",
				Password:  defaultPass,
				FirstName: "user1",
				LastName:  "test",
				Role:      3,
				Gcm:       "3234567890",
				Apn:       "0987654323",
				Active:    1,
				RegBy:     "",
			},
			{
				Source:    "local",
				Username:  "user002",
				Email:     "user2@test.com",
				Password:  defaultPass,
				FirstName: "user2",
				LastName:  "test",
				Role:      3,
				Gcm:       "4234567890",
				Apn:       "0987654324",
				Active:    2,
				RegBy:     "system",
			},
		}
		//Create users
		utils.InitLog()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}
		u := new(access.User)

		db.Users(dbses).DropCollection()
		i := []string{}
		for _, v := range tt2 {
			regreq := v
			p := access.CreateUser{
				Dbsess: dbsess,
				Req:    regreq,
			}

			uc, err := u.Create(p)
			if err != nil {
				t.Fatalf("failed create user: %v", err)
			}
			u = uc.(*access.User)
			i = append(i, u.ID)
			ucheck, err := access.UserByID(dbses, u.ID, bson.M{})
			So(ucheck.Username, ShouldEqual, v.Username)
		}

		//Read single user
		Convey("success: read user by id", func() {
			for k, v := range i {
				c, err := u.Read(dbsess, v)
				So(err, ShouldBeNil)
				if err != nil {
					t.Fatalf("failed read user: %v", err)
				}
				uc := c.(*access.User)
				So(uc.Email, ShouldEqual, tt2[k].Email)
				So(uc.Username, ShouldEqual, tt2[k].Username)
				So(uc.FirstName, ShouldEqual, tt2[k].FirstName)
				So(uc.LastName, ShouldEqual, tt2[k].LastName)
				So(uc.Gcm, ShouldContain, tt2[k].Gcm)
				So(uc.Apn, ShouldContain, tt2[k].Apn)
				if tt2[k].RegBy == "system" {
					So(uc.Active, ShouldEqual, tt2[k].Active)
					So(uc.Role, ShouldEqual, tt2[k].Role)
				} else {
					So(uc.Role, ShouldEqual, 3)
					So(uc.Active, ShouldEqual, 1)
				}
			}
		})

		Convey("failed: unregistered id", func() {
			_, err := u.Read(dbsess, "1")
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "user not found")
		})

		//Read multiple users
		Convey("success read users with param", func() {
			type TP struct {
				number   int
				params   access.UserReadParam
				len      int
				email    []string
				username []string
			}
			tp := []TP{
				{ //read param: single id
					number: 1,
					params: access.UserReadParam{
						ID: i[:1],
					},
					len:      1,
					email:    []string{tt2[0].Email},
					username: []string{tt2[0].Username},
				},
				{ //read param: multiple ids
					number: 2,
					params: access.UserReadParam{
						ID: i,
					},
					len:      len(i),
					email:    []string{tt2[0].Email, tt2[1].Email, tt2[2].Email, tt2[3].Email},
					username: []string{tt2[0].Username, tt2[1].Username, tt2[2].Username, tt2[3].Username},
				},
				{ //read param: email
					number: 3,
					params: access.UserReadParam{
						Email: tt2[3].Email,
					},
					len:      1,
					email:    []string{tt2[3].Email},
					username: []string{tt2[3].Username},
				},
				{ //read param: username
					number: 4,
					params: access.UserReadParam{
						Username: tt2[2].Username,
					},
					len:      1,
					email:    []string{tt2[2].Email},
					username: []string{tt2[2].Username},
				},
				{ //read param: role
					number: 5,
					params: access.UserReadParam{
						Role: 3,
					},
					len:      2,
					email:    []string{tt2[2].Email, tt2[3].Email},
					username: []string{tt2[2].Username, tt2[3].Username},
				},
				{ //read param: active
					number: 6,
					params: access.UserReadParam{
						Active: 1,
					},
					len:      2,
					email:    []string{tt2[0].Email, tt2[2].Email},
					username: []string{tt2[0].Username, tt2[2].Username},
				},
				{ //read param: confirm
					number: 7,
					params: access.UserReadParam{
						Confirmed: 1,
					},
					len:      3,
					email:    []string{tt2[0].Email, tt2[1].Email, tt2[3].Email},
					username: []string{tt2[0].Username, tt2[1].Username, tt2[3].Username},
				},
				{ //read param: regby
					number: 7,
					params: access.UserReadParam{
						Regby: "system",
					},
					len:      3,
					email:    []string{tt2[0].Email, tt2[1].Email, tt2[3].Email},
					username: []string{tt2[0].Username, tt2[1].Username, tt2[3].Username},
				},
				//- page
			}
			for _, v := range tp {
				p := access.UserReadParamReq{
					Dbsess: dbsess,
					Req:    v.params,
				}
				c, err := u.ReadWParam(p)
				So(err, ShouldBeNil)
				if err != nil {
					t.Fatalf("failed read users: %v", err)
				}
				pres := c.(*utils.Pagination)
				it := pres.Items.(*[]access.User)
				So(len(*it), ShouldEqual, v.len)
				for k, _ := range *it {
					So((*it)[k].Email, ShouldEqual, v.email[k])
					So((*it)[k].Username, ShouldEqual, v.username[k])
				}
			}
		})

	})
}
