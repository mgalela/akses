package apptest

import (
	"testing"

	"github.com/kataras/iris"
	"github.com/mgalela/akses/access"
	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
	mgo "gopkg.in/mgo.v2"

	. "github.com/smartystreets/goconvey/convey"
)

var (
	ttapi = []access.ApiCreateParam{
		{
			Code: "api_add",
			Desc: "api add",
			URI:  "/auth/api/add",
		},
		{
			Code: "api_read",
			Desc: "api read",
			URI:  "/auth/api/read",
		},
		{
			Code: "api_update",
			Desc: "api update",
			URI:  "/auth/api/update",
		},
		{
			Code: "api_delete",
			Desc: "api delete",
			URI:  "/auth/api/delete",
		},
	}
)

func TestApiCreate(t *testing.T) {
	Convey("Test Api Create", t, func() {
		ttapi1 := []access.ApiCreateParam{
			{
				Code: "api_add", //code exist
				Desc: "api add",
				URI:  "/auth/api/add",
			},
			{
				Code: "", //code invalid
				Desc: "api add",
				URI:  "/auth/api/add",
			},
			{
				Code: "api_adda",
				Desc: "", //desc invalid
				URI:  "/auth/api/add",
			},
			{
				Code: "api_adda",
				Desc: "api add",
				URI:  "", //URI invalid
			},
		}
		utils.InitLog()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}

		Convey("Succeed", func() {
			db.API(dbses).DropCollection()
			for _, v := range ttapi {
				regreq := v
				p := access.ApiCreateRequest{
					Dbsess: dbsess,
					Req:    regreq,
				}
				u := new(access.API)
				uc, err := u.Create(p)
				if err != nil {
					t.Fatalf("failed create api: %v", err)
				}
				ucheck := uc.(*access.API)
				err = db.API(dbses).FindId(ucheck.ID).One(u)
				So(u.Code, ShouldEqual, v.Code)
				So(u.Desc, ShouldEqual, v.Desc)
				So(u.URI, ShouldEqual, v.URI)
			}
		})
		Convey("Failed: code exist", func() {
			regreq := ttapi1[0]
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.API)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "Duplicate API")
		})

		Convey("Failed: code invalid", func() {
			regreq := ttapi1[1]
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.API)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "invalid param")
		})

		Convey("Failed: desc invalid", func() {
			regreq := ttapi1[2]
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.API)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "invalid param")
		})

		Convey("Success: URI invalid", func() {
			regreq := ttapi1[3]
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.API)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "invalid param")
		})

		Reset(func() {
			dbses.Close()
		})
	})
}

func TestApiRead(t *testing.T) {
	Convey("Test Api Read", t, func() {
		utils.InitLog()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}
		u := new(access.API)

		db.API(dbses).DropCollection()
		i := []string{}
		for _, v := range ttapi {
			regreq := v
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    regreq,
			}

			uc, err := u.Create(p)
			if err != nil {
				t.Fatalf("failed create api: %v", err)
			}
			u = uc.(*access.API)
			i = append(i, u.ID)
			err = db.API(dbses).FindId(u.ID).One(u)
			So(u.Code, ShouldEqual, v.Code)
		}
		Convey("Read single id success", func() {
			for k, v := range i {
				c, err := u.Read(dbsess, v)
				So(err, ShouldBeNil)
				if err != nil {
					t.Fatalf("failed read user: %v", err)
				}
				uc := c.(*access.API)
				So(uc.Code, ShouldEqual, ttapi[k].Code)
				So(uc.Desc, ShouldEqual, ttapi[k].Desc)
				So(uc.URI, ShouldEqual, ttapi[k].URI)
			}
		})
		Convey("Failed: Read single id not found", func() {
			_, err := u.Read(dbsess, "1")
			So(err, ShouldNotBeNil)
			So(err, ShouldEqual, mgo.ErrNotFound)
		})
		//Read multiple apis
		Convey("success read apis with param", func() {
			type TP struct {
				number int
				params access.ApiReadParam
				len    int
				code   []string
				desc   []string
				uri    []string
			}
			tp := []TP{
				{ //read param: single id
					number: 1,
					params: access.ApiReadParam{
						ID: i[:1],
					},
					len:  1,
					code: []string{ttapi[0].Code},
					desc: []string{ttapi[0].Desc},
					uri:  []string{ttapi[0].URI},
				},
				{ //read param: multiple ids
					number: 2,
					params: access.ApiReadParam{
						ID: i,
					},
					len:  len(i),
					code: []string{ttapi[0].Code, ttapi[1].Code, ttapi[2].Code, ttapi[3].Code},
					desc: []string{ttapi[0].Desc, ttapi[1].Desc, ttapi[2].Desc, ttapi[3].Desc},
					uri:  []string{ttapi[0].URI, ttapi[1].URI, ttapi[2].URI, ttapi[3].URI},
				},
				{ //read param: code
					number: 3,
					params: access.ApiReadParam{
						Code: ttapi[3].Code,
					},
					len:  1,
					code: []string{ttapi[3].Code},
					desc: []string{ttapi[3].Desc},
					uri:  []string{ttapi[3].URI},
				},
				{ //read param: URI
					number: 4,
					params: access.ApiReadParam{
						URI: ttapi[2].URI,
					},
					len:  1,
					code: []string{ttapi[2].Code},
					desc: []string{ttapi[2].Desc},
					uri:  []string{ttapi[2].URI},
				},
			}
			for _, v := range tp {
				p := access.ApiReadReq{
					Dbsess: dbsess,
					Req:    v.params,
				}
				c, err := u.ReadWParam(p)
				So(err, ShouldBeNil)
				if err != nil {
					t.Fatalf("failed read apis: %v", err)
				}
				pres := c.(*utils.Pagination)
				it := pres.Items.(*[]access.API)
				So(len(*it), ShouldEqual, v.len)
				for k, _ := range *it {
					So((*it)[k].Code, ShouldEqual, v.code[k])
					So((*it)[k].Desc, ShouldEqual, v.desc[k])
					So((*it)[k].URI, ShouldEqual, v.uri[k])
				}
			}
		})

		Reset(func() {
			db.API(dbses).DropCollection()
			dbses.Close()
		})

	})
}

func TestApiDelete(t *testing.T) {
	Convey("Test Api Delete", t, func() {
		utils.InitLog()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}
		u := new(access.API)

		db.API(dbses).DropCollection()
		i := []string{}
		for _, v := range ttapi {
			regreq := v
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    regreq,
			}

			uc, err := u.Create(p)
			if err != nil {
				t.Fatalf("failed create api: %v", err)
			}
			u = uc.(*access.API)
			i = append(i, u.ID)
			err = db.API(dbses).FindId(u.ID).One(u)
			So(u.Code, ShouldEqual, v.Code)
		}
		Convey("Delete success", func() {
			res, err := u.Delete(dbsess, i[1])
			So(err, ShouldBeNil)
			So(res, ShouldEqual, 0)
			err = db.API(dbses).FindId(i[1]).One(u)
			So(res, ShouldNotBeNil)
			So(err, ShouldEqual, mgo.ErrNotFound)
		})
		Convey("Failed: ID not found", func() {
			res, err := u.Delete(dbsess, "1")
			So(err, ShouldNotBeNil)
			So(res, ShouldEqual, iris.StatusNotFound)
		})

		Reset(func() {
			db.API(dbses).DropCollection()
			dbses.Close()
		})

	})
}

func TestApiUpdate(t *testing.T) {
	Convey("Test Api Update", t, func() {
		ttapiupdate := []access.ApiUpdateParam{
			{
				Code: "api_add1",
				Desc: "api add1",
				URI:  "/auth/api/add1",
			},
			{
				Code: "api_add2",
				Desc: "",
				URI:  "",
			},
			{
				Code: "",
				Desc: "api add2",
				URI:  "",
			},
			{
				Code: "",
				Desc: "",
				URI:  "/auth/api/add2",
			},
		}
		ttapiupdate1 := access.ApiUpdateParam{
			Code: "api_read", //code duplicate
			Desc: "api read",
			URI:  "/auth/api/read",
		}

		utils.InitLog()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}
		db.API(dbses).DropCollection()
		u := new(access.API)

		i := []string{}
		for _, v := range ttapi {
			regreq := v
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    regreq,
			}

			uc, err := u.Create(p)
			if err != nil {
				t.Fatalf("failed create api: %v", err)
			}
			u = uc.(*access.API)
			i = append(i, u.ID)
			err = db.API(dbses).FindId(u.ID).One(u)
			So(u.Code, ShouldEqual, v.Code)
		}
		Convey("Success", func() {
			for k, v := range ttapiupdate {
				updt := access.ApiUpdateReq{
					Dbsess: dbsess,
					Req:    v,
				}
				err := u.Update(updt, i[0])
				So(err, ShouldBeNil)
				err = db.API(dbses).FindId(i[0]).One(u)
				if k == 0 {
					So(u.Code, ShouldEqual, v.Code)
					So(u.Desc, ShouldEqual, v.Desc)
					So(u.URI, ShouldEqual, v.URI)
				} else if k == 1 {
					So(u.Code, ShouldEqual, v.Code)
				} else if k == 2 {
					So(u.Desc, ShouldEqual, v.Desc)
				} else if k == 3 {
					So(u.URI, ShouldEqual, v.URI)
				}
			}
		})
		Convey("Failed: ID not found", func() {
			updt := access.ApiUpdateReq{
				Dbsess: dbsess,
				Req:    ttapiupdate[1],
			}
			err := u.Update(updt, "1")
			So(err, ShouldNotBeNil)
			So(err, ShouldEqual, mgo.ErrNotFound)
		})
		Convey("Failed: Duplicate API", func() {
			updt := access.ApiUpdateReq{
				Dbsess: dbsess,
				Req:    ttapiupdate1,
			}
			err := u.Update(updt, i[2])
			So(err, ShouldNotBeNil)
			So(err.Error(), ShouldEqual, "Duplicate API")
		})

		Reset(func() {
			db.API(dbses).DropCollection()
			dbses.Close()
		})

	})
}
