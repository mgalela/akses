package apptest

import (
	"testing"

	"github.com/kataras/iris"
	"github.com/mgalela/akses/access"
	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
	mgo "gopkg.in/mgo.v2"

	. "github.com/smartystreets/goconvey/convey"
)

var (
	ttgroup = []access.GroupCreateParam{
		{
			//ID:   "1",
			Role: 1,
			Name: "admin",
			Apis: []string{},
		},
		{
			//ID:   "2",
			Role: 2,
			Name: "opr",
			Apis: []string{},
		},
		{
			//ID:   "3",
			Role: 3,
			Name: "user",
			Apis: []string{},
		},
	}
	ttgroup1 = []access.GroupCreateParam{
		{
			//ID:   "1",
			Role: 1,
			Name: "admin",
			Apis: []string{},
		},
		{
			//ID:   "2",
			Role: 2,
			Name: "opr",
			Apis: []string{},
		},
		{
			//ID:   "3",
			Role: 3,
			Name: "user",
			Apis: []string{},
		},
	}
)

func TestGroupCreate(t *testing.T) {
	Convey("Test Group Create", t, func() {
		utils.InitLog()
		access.InitAccessControl()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}

		//Create API
		apiids := []string{}
		//apis := []access.API{}
		db.API(dbses).DropCollection()
		for _, v := range ttapi {
			regreq := v
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    regreq,
			}
			u := new(access.API)
			uc, err := u.Create(p)
			if err != nil {
				t.Fatalf("failed create api: %v", err)
			}
			ucheck := uc.(*access.API)
			if err != nil {
				t.Fatalf("failed create api: %v", err)
			}
			err = db.API(dbses).FindId(ucheck.ID).One(u)
			apiids = append(apiids, ucheck.ID)
			//apis = append(apis, *ucheck)
		}

		Convey("Succeed", func() {
			db.UserGroup(dbses).DropCollection()
			for _, v := range ttgroup {
				req := v
				req.Apis = apiids
				p := access.GroupCreateRequest{
					Dbsess: dbsess,
					Req:    req,
				}
				g := new(access.Group)
				uc, err := g.Create(p)
				if err != nil {
					t.Fatalf("failed create group: %v", err)
				}
				gc := uc.(*access.Group)
				err = db.UserGroup(dbses).FindId(gc.ID).One(g)
				So(g.Role, ShouldEqual, v.Role)
				So(g.Name, ShouldEqual, v.Name)
				//So(g.Apis, ShouldEqual, apis)
				So(len(g.Apis), ShouldEqual, len(apiids))
			}
		})
		Convey("Failed: role exist", func() {
			req := ttgroup1[0]
			req.Apis = apiids
			p := access.GroupCreateRequest{
				Dbsess: dbsess,
				Req:    req,
			}
			u := new(access.Group)
			_, err := u.Create(p)
			So(err, ShouldNotBeNil)
		})

		Reset(func() {
			db.API(dbses).DropCollection()
			dbses.Close()
		})
	})
}

func TestGroupDelete(t *testing.T) {
	Convey("Test Group Delete", t, func() {
		utils.InitLog()
		access.InitAccessControl()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}

		//create API
		db.API(dbses).DropCollection()
		apiids := []string{}
		for _, v := range ttapi {
			req := v
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    req,
			}
			a := new(access.API)
			ac, err := a.Create(p)
			if err != nil {
				t.Fatalf("failed create api: %v", err)
			}
			acheck := ac.(*access.API)
			err = db.API(dbses).FindId(acheck.ID).One(a)
			apiids = append(apiids, acheck.ID)
		}

		//Create Group
		db.UserGroup(dbses).DropCollection()
		gids := []string{}
		for _, v := range ttgroup {
			req := v
			req.Apis = apiids
			//t.Logf("req.APis %v", req.Apis)
			p := access.GroupCreateRequest{
				Dbsess: dbsess,
				Req:    req,
			}
			g := new(access.Group)
			gc, err := g.Create(p)
			if err != nil {
				t.Fatalf("failed create group: %v", err)
			}
			gcheck := gc.(*access.Group)
			err = db.UserGroup(dbses).FindId(gcheck.ID).One(g)
			//t.Logf("g.APis %v", g.Apis)
			gids = append(gids, g.ID)
			So(g.Role, ShouldEqual, v.Role)
			So(g.Name, ShouldEqual, v.Name)
			So(len(g.Apis), ShouldEqual, len(apiids))
		}
		Convey("Delete success", func() {
			g := new(access.Group)
			res, err := g.Delete(dbsess, gids[0])
			So(err, ShouldBeNil)
			So(res, ShouldEqual, 0)
			err = db.UserGroup(dbses).FindId(gids[0]).One(g)
			So(res, ShouldNotBeNil)
			So(err, ShouldEqual, mgo.ErrNotFound)
		})
		Convey("Failed: ID not found", func() {
			g := new(access.Group)
			res, err := g.Delete(dbsess, "99")
			So(err, ShouldNotBeNil)
			So(res, ShouldEqual, iris.StatusNotFound)
		})

		Reset(func() {
			db.API(dbses).DropCollection()
			db.UserGroup(dbses).DropCollection()
			dbses.Close()
		})

	})
}

func TestGroupRead(t *testing.T) {
	Convey("Test Group Read", t, func() {
		utils.InitLog()
		access.InitAccessControl()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}

		//create API
		db.API(dbses).DropCollection()
		apiids := []string{}
		for _, v := range ttapi {
			req := v
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    req,
			}
			a := new(access.API)
			ac, err := a.Create(p)
			if err != nil {
				t.Fatalf("failed create api: %v", err)
			}
			acheck := ac.(*access.API)
			err = db.API(dbses).FindId(acheck.ID).One(a)
			apiids = append(apiids, acheck.ID)
		}

		//Create Group
		db.UserGroup(dbses).DropCollection()
		gids := []string{}
		for _, v := range ttgroup {
			req := v
			req.Apis = apiids
			//t.Logf("req.APis %v", req.Apis)
			p := access.GroupCreateRequest{
				Dbsess: dbsess,
				Req:    req,
			}
			g := new(access.Group)
			gc, err := g.Create(p)
			if err != nil {
				t.Fatalf("failed create group: %v", err)
			}
			gcheck := gc.(*access.Group)
			err = db.UserGroup(dbses).FindId(gcheck.ID).One(g)
			//t.Logf("g.APis %v", g.Apis)
			gids = append(gids, g.ID)
			So(g.Role, ShouldEqual, v.Role)
			So(g.Name, ShouldEqual, v.Name)
			So(len(g.Apis), ShouldEqual, len(apiids))
		}
		Convey("Read single id success", func() {
			g := new(access.Group)
			for k, v := range gids {
				c, err := g.Read(dbsess, v)
				So(err, ShouldBeNil)
				if err != nil {
					t.Fatalf("failed read user: %v", err)
				}
				uc := c.(*access.Group)
				So(uc.Role, ShouldEqual, ttgroup[k].Role)
				So(uc.Name, ShouldEqual, ttgroup[k].Name)
				//So(uc.URI, ShouldEqual, ttapi[k].URI)
			}
		})
		Convey("Failed: Read single id not found", func() {
			g := new(access.Group)
			_, err := g.Read(dbsess, "99")
			So(err, ShouldNotBeNil)
			So(err, ShouldEqual, mgo.ErrNotFound)
		})
		//Read multiple groups
		Convey("success read groups with param", func() {
			type TP struct {
				number int
				params access.GroupReadParam
				len    int
				role   []int
				name   []string
			}
			tp := []TP{
				{ //read param: single id
					number: 1,
					params: access.GroupReadParam{
						ID: gids[:1],
					},
					len:  1,
					role: []int{ttgroup[0].Role},
					name: []string{ttgroup[0].Name},
				},
				{ //read param: multiple ids
					number: 2,
					params: access.GroupReadParam{
						ID: gids,
					},
					len:  len(gids),
					role: []int{ttgroup[0].Role, ttgroup[1].Role, ttgroup[2].Role},
					name: []string{ttgroup[0].Name, ttgroup[1].Name, ttgroup[2].Name},
				},
				{ //read param: role
					number: 3,
					params: access.GroupReadParam{
						Role: ttgroup[2].Role,
					},
					len:  1,
					role: []int{ttgroup[2].Role},
					name: []string{ttgroup[2].Name},
				},
				{ //read param: URI
					number: 4,
					params: access.GroupReadParam{
						Name: ttgroup[1].Name,
					},
					len:  1,
					role: []int{ttgroup[1].Role},
					name: []string{ttgroup[1].Name},
				},
			}
			g := new(access.Group)
			for _, v := range tp {
				p := access.GroupReadReq{
					Dbsess: dbsess,
					Req:    v.params,
				}
				c, err := g.ReadWParam(p)
				So(err, ShouldBeNil)
				if err != nil {
					t.Fatalf("failed read groups: %v", err)
				}
				pres := c.(*utils.Pagination)
				it := pres.Items.(*[]access.Group)
				So(len(*it), ShouldEqual, v.len)
				for k, _ := range *it {
					So((*it)[k].Role, ShouldEqual, v.role[k])
					So((*it)[k].Name, ShouldEqual, v.name[k])
				}
			}
		})

		Reset(func() {
			db.API(dbses).DropCollection()
			db.UserGroup(dbses).DropCollection()
			dbses.Close()
		})

	})
}

func TestGroupUpdate(t *testing.T) {
	Convey("Test Group Update", t, func() {
		ttgroupupdate := []access.GroupUpdateParam{
			/*{
				ID:   "1",
				Role: 4,
			},*/
			{
				ID:   "1",
				Name: "admin1",
			},
			{
				ID:   "1",
				Apis: []string{},
			},
		}

		utils.InitLog()
		access.InitAccessControl()
		dbses, _ := db.Session()
		dbsess := access.Dbsess{
			Sess: dbses,
		}

		//create API
		db.API(dbses).DropCollection()
		apiids := []string{}
		for _, v := range ttapi {
			req := v
			p := access.ApiCreateRequest{
				Dbsess: dbsess,
				Req:    req,
			}
			a := new(access.API)
			ac, err := a.Create(p)
			if err != nil {
				t.Fatalf("failed create api: %v", err)
			}
			acheck := ac.(*access.API)
			err = db.API(dbses).FindId(acheck.ID).One(a)
			apiids = append(apiids, acheck.ID)
		}

		//Create Group
		db.UserGroup(dbses).DropCollection()
		gids := []string{}
		for _, v := range ttgroup {
			req := v
			req.Apis = apiids
			//t.Logf("req.APis %v", req.Apis)
			p := access.GroupCreateRequest{
				Dbsess: dbsess,
				Req:    req,
			}
			g := new(access.Group)
			gc, err := g.Create(p)
			if err != nil {
				t.Fatalf("failed create group: %v", err)
			}
			gcheck := gc.(*access.Group)
			err = db.UserGroup(dbses).FindId(gcheck.ID).One(g)
			//t.Logf("g.APis %v", g.Apis)
			gids = append(gids, g.ID)
			So(g.Role, ShouldEqual, v.Role)
			So(g.Name, ShouldEqual, v.Name)
			So(len(g.Apis), ShouldEqual, len(apiids))
		}
		Convey("Success", func() {
			g := new(access.Group)
			for k, v := range ttgroupupdate {
				updt := access.GroupUpdateReq{
					Dbsess: dbsess,
					Req:    v,
				}
				//t.Logf("gids %v", gids)
				err := g.Update(updt, gids[0])
				/*if k == 0 {
					So(err, ShouldBeNil)
					err = db.UserGroup(dbses).FindId(gids[0]).One(g)
					So(g.Role, ShouldEqual, v.Role)
				} else */if k == 0 {
					So(err, ShouldBeNil)
					err = db.UserGroup(dbses).FindId(gids[0]).One(g)
					So(g.Name, ShouldEqual, v.Name)
				} else if k == 1 {
					So(err, ShouldNotBeNil)
					So(err.Error(), ShouldEqual, "invalid update")
				}
			}
		})

		Reset(func() {
			db.API(dbses).DropCollection()
			db.UserGroup(dbses).DropCollection()
			dbses.Close()
		})

	})
}
