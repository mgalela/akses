package db

import (
	"os"
	"strings"

	log "github.com/Sirupsen/logrus"
	"gopkg.in/mgo.v2"
)

const (
	defaultURI = "mongodb://localhost/akses"
)

var (
	session *mgo.Session
)

//Session getDB session
func Session() (*mgo.Session, error) {
	var err error

	if session == nil {
		if session, err = mgo.Dial(getDbURI()); err != nil {
			log.Fatal("cannot connect to mongodb, reason = ", err)
			return nil, err
		}
	}
	return session.Copy(), nil
}

func getDbName() string {
	uriParts := strings.SplitN(getDbURI(), "/", 4)

	// get the last part (maybe a name with or without options)
	name := uriParts[len(uriParts)-1]

	// get the first part of name (omit options if any)
	nameParts := strings.SplitN(name, "?", 2)
	return nameParts[0]
}

func getDbURI() string {
	if mgoURI := os.Getenv("MONGODB_URI"); mgoURI != "" {
		return mgoURI
	}
	return defaultURI
}

//DB get anode DB
func DB(session *mgo.Session) *mgo.Database {
	return session.DB(getDbName())
}

//AnodeAPI get anode_api collection
func API(session *mgo.Session) *mgo.Collection {
	return DB(session).C("api")
}

//UserGroup get user group collection
func UserGroup(session *mgo.Session) *mgo.Collection {
	return DB(session).C("user_groups")
}

//Users get users collection
func Users(session *mgo.Session) *mgo.Collection {
	return DB(session).C("users")
}

//TokenCaches get token caches collection
func TokenCaches(session *mgo.Session) *mgo.Collection {
	return DB(session).C("token_caches")
}

//KeyCaches get key caches collection
func KeyCaches(session *mgo.Session) *mgo.Collection {
	return DB(session).C("key_caches")
}

func ConfirmKeyCaches(session *mgo.Session) *mgo.Collection {
	return DB(session).C("confirm_key_caches")
}

//Organizations represent organization collection
func Orgs(session *mgo.Session) *mgo.Collection {
	return DB(session).C("orgs")
}

//Jobdescs represent jobdescs collection
func Jobdescs(session *mgo.Session) *mgo.Collection {
	return DB(session).C("jobdescs")
}

//Schedules represet schedules collection
func Schedules(session *mgo.Session) *mgo.Collection {
	return DB(session).C("schedules")
}

//Readers represent readers collection
func Readers(session *mgo.Session) *mgo.Collection {
	return DB(session).C("readers")
}

//UserProfiles represent user_profiles collection
func UserProfiles(session *mgo.Session) *mgo.Collection {
	return DB(session).C("user_profiles")
}

//OrganizationTokens handle organization tokens
func OrgTokens(session *mgo.Session) *mgo.Collection {
	return DB(session).C("org_tokens")
}

func Entries(session *mgo.Session) *mgo.Collection {
	return DB(session).C("entries")
}

func Comments(session *mgo.Session) *mgo.Collection {
	return DB(session).C("comments")
}
