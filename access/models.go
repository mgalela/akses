package access

import (
	"github.com/kataras/iris"
	"gopkg.in/mgo.v2"
)

type Resp struct {
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

var (
	RespOK = Resp{Msg: "OK"}
)

//Access interface
type Access interface {
	Create(p interface{}) (interface{}, error)
	Read(p interface{}, id string) (interface{}, error)
	ReadWParam(p interface{}) (interface{}, error)
	Update(p interface{}, id string) error
	Delete(p interface{}, id string) (int, error)
}

type Dbsess struct {
	Sess *mgo.Session
}

func RespErrWithMsg(ctx iris.Context, status int, msg string) {
	ctx.StatusCode(status)
	resp := Resp{Msg: msg}
	ctx.JSON(resp)
}

func RespOkWithData(ctx iris.Context, status int, msg string, data interface{}) {
	ctx.StatusCode(status)
	resp := Resp{Msg: msg, Data: data}
	ctx.JSON(resp)
}

func RespOk(ctx iris.Context, status int) {
	ctx.StatusCode(status)
	ctx.JSON(RespOK)
}
