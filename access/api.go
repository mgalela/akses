package access

import (
	"errors"

	"github.com/kataras/iris"
	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	maxApiPerPage = 50
)

type API struct {
	ID   string `json:"id" bson:"_id"`
	Code string `json:"code" bson:"code"`
	Desc string `json:"desc" bson:"desc"`
	URI  string `json:"uri" bson:"uri"`
}

//APIRequest represent request for add new API
type ApiCreateParam struct {
	Code string `json:"code" binding:"required"`
	Desc string `json:"desc" binding:"required"`
	URI  string `json:"uri" binding:"required"`
}

type ApiCreateRequest struct {
	Dbsess Dbsess
	Req    ApiCreateParam
}

type ApiUpdateReq struct {
	Dbsess Dbsess
	Req    ApiUpdateParam
}

//APIPutRequest represent api put request
type ApiUpdateParam struct {
	ID   string `json:"id" binding:"required"`
	Code string `json:"code"`
	Desc string `json:"desc"`
	URI  string `json:"uri"`
}

type ApiReadReq struct {
	Dbsess Dbsess
	Req    ApiReadParam
	Page   int
}

type ApiReadParam struct {
	ID   []string
	Code string
	Desc string
	URI  string
}

//NewAPI create new APIObj
func (api *API) Create(p interface{}) (interface{}, error) {
	reg := p.(ApiCreateRequest)
	dbsess := reg.Dbsess.Sess
	r := reg.Req

	if r.Code == "" || r.Desc == "" || r.URI == "" {
		utils.Log.Debug("invalid param")
		return nil, errors.New("invalid param")
	}

	//validate if code is exist
	utils.Log.Debug("Validate whether API exist")
	if err := isCodeExist(dbsess, r.Code); err == nil {
		utils.Log.Debug("Duplicate API")
		return nil, errors.New("Duplicate API")
	}

	//api := new(API)
	api.Code = r.Code
	api.Desc = r.Desc
	api.URI = r.URI
	api.ID = bson.NewObjectId().Hex()

	err := db.API(dbsess).Insert(api)
	return api, err
}

//Delete method for Api
func (api *API) Delete(p interface{}, id string) (int, error) {
	utils.Log.Debug("Deleting Api")

	dbsess := p.(Dbsess)
	if err := db.API(dbsess.Sess).RemoveId(id); err != nil {
		if err == mgo.ErrNotFound {
			utils.Log.Error("Api not found")
			return iris.StatusNotFound, err
		}
		utils.Log.Error("Failed to query DB")
		return iris.StatusInternalServerError, err
	}

	utils.Log.Debug("Api Deleted")
	return 0, nil
}

func (api *API) Read(p interface{}, id string) (interface{}, error) {
	utils.Log.Info("Start API Read")

	dbsess := p.(Dbsess)

	utils.Log.Debug("querying API")
	err := db.API(dbsess.Sess).FindId(id).One(api)
	if err != nil {
		return nil, err
	}

	utils.Log.Info("End API Read")
	return api, nil
}

//ReadWParam for User
func (api *API) ReadWParam(p interface{}) (interface{}, error) {

	limit := bson.M{}
	queryPart := bson.M{}

	/*create query params*/
	req, ok := p.(ApiReadReq)
	dbsess := req.Dbsess
	q := req.Req
	page := req.Page

	if !ok {
		return nil, ErrParsing
	}

	if len(q.ID) > 0 {
		queryPart["_id"] = bson.M{"$in": q.ID}
	}

	if q.Code != "" {
		queryPart["code"] = q.Code
	}

	if q.Desc != "" {
		queryPart["desc"] = q.Desc
	}

	if q.URI != "" {
		queryPart["uri"] = q.URI
	}

	if page < 1 {
		page = 1
	}

	//do query
	utils.Log.Debug("querying api")

	u := new([]API)
	body, err := utils.Paginate(db.API(dbsess.Sess), queryPart, limit, u, page, maxApiPerPage, []string{})
	if err != nil {
		return nil, err
	}

	utils.Log.Info("End Api ReadWParam")
	return body, nil
}

func (api *API) Update(p interface{}, id string) error {
	updt := p.(ApiUpdateReq)
	dbsess := updt.Dbsess.Sess
	req := updt.Req

	/*build update data*/
	set := bson.M{}
	q := bson.M{}

	if req.Code != "" {
		//validate is exist
		//TODO: use mongodb index unique
		utils.Log.Debug("Validate whether other same Code exist")
		if err := isCodeExist(dbsess, req.Code); err == nil {
			if api.ID != id {
				utils.Log.Debug("Duplicate API")
				return errors.New("Duplicate API")
			}
		}
		set["code"] = req.Code
	}
	if req.Desc != "" {
		set["desc"] = req.Desc
	}
	if req.URI != "" {
		set["uri"] = req.URI
	}

	if len(set) == 0 {
		return errors.New("invalid update")
	}
	q["$set"] = set

	if err := db.API(dbsess).UpdateId(id, q); err != nil {
		utils.Log.Error("Failed to update api, reason=", err)
		return err
	}

	return nil
}
