package access

import (
	"strconv"

	"github.com/kataras/iris"
	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
	"github.com/mikespook/gorbac"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//NewGroup create new group
func NewGroup(session *mgo.Session, r *GroupCreateParam) (*Group, error) {
	g := new(Group)
	//g.ID = bson.NewObjectId().Hex()
	g.ID = r.Name
	g.Role = r.Role
	g.Name = r.Name

	apis, err := populateAPIBasedOnID(r.Apis, session)
	if err != nil {
		return nil, err
	}

	g.Apis = append(g.Apis, apis...)
	err = db.UserGroup(session).Insert(g)
	if err != nil {
		return nil, err
	}

	//populate rbac
	Roles[g.Role] = gorbac.NewStdRole(strconv.Itoa(g.Role))
	for _, val := range g.Apis {
		Roles[g.Role].Assign(gorbac.NewStdPermission(val.URI))
	}
	Rbac.Add(Roles[g.Role])

	return g, nil
}

//AddNewGroup handle add new group
func GroupCreate(ctx iris.Context) {
	var groupRequest GroupCreateParam

	utils.Log.Info("Starting Add new group")
	session := ctx.Values().Get("mongosession").(*mgo.Session)

	utils.Log.Info("Binding group request")
	if err := ctx.ReadForm(&groupRequest); err != nil {
		utils.Log.Error("Error when binding request, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	//TODO: implement upsert:true, and $setOnInsert
	//find is group exist
	utils.Log.Debug("Validate user group")
	group := Group{}
	if err := db.UserGroup(session).Find(bson.M{"name": groupRequest.Name}).One(&group); err == nil {
		utils.Log.Debug("Duplicate Group Name")
		RespErrWithMsg(ctx, iris.StatusBadRequest, "Duplicate Group Name")
		return
	}

	//insert it
	utils.Log.Debug("insert new group")
	if _, err := NewGroup(session, &groupRequest); err != nil {
		utils.Log.Error("Failed to query DB, err=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End add new group")
	RespOk(ctx, iris.StatusCreated)
}

//UpdateGroup update group by ID
func GroupUpdate(ctx iris.Context) {
	var groupRequest GroupUpdateParam

	utils.Log.Info("Starting Add new group")
	session := ctx.Values().Get("mongosession").(*mgo.Session)

	utils.Log.Info("Binding group request")
	if err := ctx.ReadForm(&groupRequest); err != nil {
		utils.Log.Error("Error when binding request, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}
	groupID := groupRequest.ID

	//validate whether group exist
	utils.Log.Debug("validate group exist")
	g := Group{}
	if err := db.UserGroup(session).Find(bson.M{"_id": bson.M{"$ne": groupID}, "name": groupRequest.Name}).One(&g); err == nil {
		utils.Log.Debug("Duplicate Group")
		RespErrWithMsg(ctx, iris.StatusBadRequest, "Duplicate Group")
		return
	}

	apis, err := populateAPIBasedOnPutReq(&groupRequest, session)
	if err != nil {
		utils.Log.Error("Internal Server error, err=" + err.Error())
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	updtGroup := bson.M{
		"name": groupRequest.Name,
		"role": groupRequest.Role,
		"apis": apis,
	}

	utils.Log.Debug("Do update group")
	if err := db.UserGroup(session).UpdateId(groupID, bson.M{"$set": updtGroup}); err != nil {
		if err == mgo.ErrNotFound {
			utils.Log.Info("Group Not found")
			RespErrWithMsg(ctx, iris.StatusNotFound, "Group not found")
			return
		}

		utils.Log.Error("Failed to query DB,err=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	//update group of user
	if err := updateUserByNewGroup(session, groupID, groupRequest.Name, apis); err != nil {
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	//do repopulate access control
	Rbac.Remove(Roles[groupRequest.Role].ID())
	Roles[groupRequest.Role] = gorbac.NewStdRole(strconv.Itoa(groupRequest.Role))
	for _, val := range apis {
		Roles[groupRequest.Role].Assign(gorbac.NewStdPermission(val.URI))
	}
	Rbac.Add(Roles[groupRequest.Role])

	utils.Log.Info("End update group")
	RespOk(ctx, iris.StatusOK)
}

//GetGroup handles get group by specific query
func GroupRead(ctx iris.Context) {
	var groupName string
	var groupRole string
	var groupID string

	utils.Log.Info("Start Get group")
	session := ctx.Values().Get("mongosession").(*mgo.Session)

	utils.Log.Debug("get query string")
	//populate val based on query string
	groupName = ctx.Params().Get("name")
	groupID = ctx.Params().Get("id")
	groupRole = ctx.Params().Get("role")

	//build query
	utils.Log.Debug("build query part")
	queryPart := bson.M{}
	if groupName != "" {
		queryPart["name"] = groupName
	}

	if role, err := strconv.Atoi(groupRole); err != nil {
		RespErrWithMsg(ctx, iris.StatusBadRequest, "role must be integer value")
		return
	} else {
		queryPart["role"] = role
	}

	if groupID != "" {
		queryPart["_id"] = groupID
	}

	//execute
	utils.Log.Debug("execute query")
	groups := []Group{}
	if err := db.UserGroup(session).Find(queryPart).All(&groups); err != nil {
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}
	RespOkWithData(ctx, iris.StatusOK, "OK", groups)
}

//DeleteGroup Handle delete group
func GroupDelete(ctx iris.Context) {
	utils.Log.Info("Start Delete Group")

	session := ctx.Values().Get("mongosession").(*mgo.Session)
	var id string

	//get param
	utils.Log.Debug("get query string")

	if id = ctx.Params().Get("id"); id == "" {
		utils.Log.Error("id required")
		RespErrWithMsg(ctx, iris.StatusBadRequest, "id required")
		return
	}

	//TODO
	//do delete group
	var u Access
	rsp, err := u.Delete(session, id)
	if err != nil {
		RespErrWithMsg(ctx, rsp, err.Error())
		return
	}

	utils.Log.Info("End Delete Group")
	RespOk(ctx, iris.StatusOK)
}
