package access

import (
	"encoding/json"
	"errors"
	"net/http"
	//"gopkg.in/mgo.v2"
	//"gopkg.in/mgo.v2/bson"
	//"github.com/mgalela/akses/appserver/db"
)

const (
	graphPrefixURI = "https://graph.facebook.com"
)

var (
	socialAccountMap = map[string]string{
		"facebook": "Facebook",
		"google":   "Google",
		"twitter":  "Twitter",
	}
)

//FacebookPicture represent facebook response when requesting picture
type FacebookPicture struct {
	Data facebookResponseData `json:"data"`
}

type facebookResponseData struct {
	Url string `json:"url"`
}

type facebookUser struct {
	ID        string `json:"id"`
	Email     string `json:"email"`
	FirstName string `json:"-"`
	LastName  string `json:"-"`
}

//RequestPicturePath handles request profile picture path
func RequestPicturePath(facebookID string) (*FacebookPicture, error) {
	uri := graphPrefixURI + "/v2.3/" + facebookID + "/picture?width=140&height=140&redirect=false"

	resp, err := http.Get(uri)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("failed to get uri")
	}

	fbPict := FacebookPicture{}
	if err := json.NewDecoder(resp.Body).Decode(&fbPict); err != nil {
		return nil, err
	}
	return &fbPict, nil
}

func getFacebookUser(token string) (*facebookUser, error) {
	fbUser := facebookUser{}
	url := "https://graph.facebook.com/v2.2/me?access_token=" + token

	r, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	if r.StatusCode != 200 {
		return nil, errors.New("failed to sync Facebook account")
	}

	if err := json.NewDecoder(r.Body).Decode(&fbUser); err != nil {
		return nil, err
	}

	return &fbUser, nil
}
