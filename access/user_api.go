package access

import (
	"strconv"
	"strings"

	"github.com/kataras/iris"
	"github.com/mgalela/akses/utils"
	mgo "gopkg.in/mgo.v2"
)

//UserCreate handle register  new user
func UserCreate(ctx iris.Context) {
	utils.Log.Info("Start Register ")

	var registerRequest UserCreateRequest
	var err error
	session := ctx.Values().Get("mongosession").(*mgo.Session)
	dbsess := Dbsess{
		Sess: session,
	}

	/*Validate and verify*/
	utils.Log.Debug("Bind request")
	//bind
	if err := ctx.ReadForm(&registerRequest); err != nil {
		utils.Log.Error("Error when binding request, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	utils.Log.Debug("saving user")
	var u Access
	p := CreateUser{
		Dbsess: dbsess,
		Req:    registerRequest,
	}
	_, err = u.Create(p)
	if err != nil {
		utils.Log.Error("Registration failed, reason=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	//	go sendConfirmationEmail(registerRequest.Email, u.ConfirmationKey, o.Name)

	utils.Log.Info("End registration")
	RespOk(ctx, iris.StatusCreated)
}

//UserRead API implementation
func UserRead(ctx iris.Context) {
	utils.Log.Info("Start UserRead")
	session := ctx.Values().Get("mongosession").(*mgo.Session)

	id := ctx.Params().Get("id") //get id param
	dbsess := Dbsess{
		Sess: session,
	}

	utils.Log.Debug("Do query user")
	var u Access
	us, err := u.Read(dbsess, id)
	if err != nil {
		utils.Log.Error(err.Error())
		if err == mgo.ErrNotFound {
			RespErrWithMsg(ctx, iris.StatusNotFound, err.Error())
			return
		}
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End UserRead")
	RespOkWithData(ctx, iris.StatusOK, "OK", us)
}

//UserReadWParam API implementation
func UserReadWParam(ctx iris.Context) {
	utils.Log.Info("Start Query user")
	session := ctx.Values().Get("mongosession").(*mgo.Session)

	var err error
	req := UserReadParamReq{}
	req.Dbsess.Sess = session
	rParam := req.Req

	q := ctx.URLParams() //get param

	/* Validate query params */
	if val, ok := q["id"]; ok {
		rParam.ID = strings.Split(val, ",")
	}

	if val, ok := q["email"]; ok {
		rParam.Email = val
	}
	if val, ok := q["username"]; ok {
		rParam.Username = val
	}

	//TODO: user hanya bisa query ke role yang sama atau lebih tinggi dari dia
	if val, ok := q["role"]; ok {
		if rParam.Role, err = strconv.Atoi(val); err != nil {
			RespErrWithMsg(ctx, iris.StatusBadRequest, "role must be integer value")
			return
		}
	}

	if val, ok := q["active"]; ok {
		if rParam.Active, err = strconv.Atoi(val); err != nil {
			RespErrWithMsg(ctx, iris.StatusBadRequest, "active must be integer value")
			return
		}
	}

	if val, ok := q["confirmed"]; ok {
		if rParam.Confirmed, err = strconv.Atoi(val); err != nil {
			RespErrWithMsg(ctx, iris.StatusBadRequest, "confirmed must be integer value")
			return
		}
	}

	if val, ok := q["regby"]; ok {
		rParam.Regby = val
	}

	if val, ok := q["page"]; ok {
		if req.Page, err = strconv.Atoi(val); err != nil {
			RespErrWithMsg(ctx, iris.StatusBadRequest, "page must be integer value")
			return
		}
	}

	utils.Log.Debug("Do query users")
	var u Access
	body, err := u.ReadWParam(rParam)
	if err != nil {
		utils.Log.Error("failed to query DB=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End UserReadWParam")
	RespOkWithData(ctx, iris.StatusOK, "OK", body)
}

//UserUpdate API implementation
func UserUpdate(ctx iris.Context) {
	utils.Log.Info("Start User Update ")

	var req UserUpdateReq
	var err error
	id := ""

	if id = ctx.Params().Get("id"); id == "" {
		utils.Log.Error("id required")
		RespErrWithMsg(ctx, iris.StatusBadRequest, "id required")
		return
	}

	session := ctx.Values().Get("mongosession").(*mgo.Session)
	dbsess := Dbsess{
		Sess: session,
	}

	/*Validate and verify*/
	utils.Log.Debug("Bind request")
	//bind
	if err := ctx.ReadForm(&req); err != nil {
		utils.Log.Error("Error when binding request, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	if req.ID != id {
		utils.Log.Error("id not matched")
		RespErrWithMsg(ctx, iris.StatusBadRequest, "id not matched")
		return
	}

	p := UpdateUser{
		Dbsess: dbsess,
		Req:    req,
	}

	utils.Log.Debug("updating user")

	var u Access
	err = u.Update(p, id)
	if err != nil {
		utils.Log.Error("Update failed, reason=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End User Update")
	RespOk(ctx, iris.StatusOK)
}

//UserDelete Handle delete user
func UserDelete(ctx iris.Context) {
	utils.Log.Info("Start Delete User")
	session := ctx.Values().Get("mongosession").(*mgo.Session)

	var id string

	//get param
	utils.Log.Debug("get query string")

	if id = ctx.Params().Get("id"); id == "" {
		utils.Log.Error("id required")
		RespErrWithMsg(ctx, iris.StatusBadRequest, "id required")
		return
	}

	//do delete user
	var u Access
	rsp, err := u.Delete(session, id)
	if err != nil {
		RespErrWithMsg(ctx, rsp, err.Error())
		return
	}

	utils.Log.Info("End Delete User")
	RespOk(ctx, iris.StatusOK)
}
