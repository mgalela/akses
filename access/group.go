package access

import (
	"errors"
	"strconv"

	"github.com/kataras/iris"
	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
	"github.com/mikespook/gorbac"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	maxGroupPerPage = 50
)

//Group represent user group
type Group struct {
	ID   string `json:"id" bson:"_id"`
	Role int    `json:"role,string" bson:"role"`
	Name string `json:"name" bson:"name"`
	Apis []API  `json:"apis" bson:"apis"`
}

//GroupRequest represent add new group request
type GroupCreateParam struct {
	Role int      `json:"role,string" binding:"required"`
	Name string   `json:"name" bson:"name" binding:"required"`
	Apis []string `json:"apis" bson:"apis"`
}

//GroupRequestPut represent update group
type GroupUpdateParam struct {
	ID   string   `json:"id" binding:"required"`
	Role int      `json:"role,string"`
	Name string   `json:"name"`
	Apis []string `json:"apis"`
}

type GroupCreateRequest struct {
	Dbsess Dbsess
	Req    GroupCreateParam
}

type GroupUpdateReq struct {
	Dbsess Dbsess
	Req    GroupUpdateParam
}

type GroupReadReq struct {
	Dbsess Dbsess
	Req    GroupReadParam
	Page   int
}

type GroupReadParam struct {
	ID   []string
	Role int `json:"role,string"`
	Name string
}

func (g *Group) Create(p interface{}) (interface{}, error) {
	req := p.(GroupCreateRequest)
	dbsess := req.Dbsess.Sess
	r := req.Req

	//g := new(Group)
	g.ID = strconv.Itoa(r.Role) //bson.NewObjectId().Hex()
	g.Role = r.Role
	g.Name = r.Name

	apis, err := populateAPIBasedOnID(r.Apis, dbsess)
	if err != nil {
		return nil, err
	}

	g.Apis = append(g.Apis, apis...)
	err = db.UserGroup(dbsess).Insert(g)
	if err != nil {
		return nil, err
	}

	//populate rbac
	//TODO: handled by other service
	Roles[g.Role] = gorbac.NewStdRole(strconv.Itoa(g.Role))
	for _, val := range g.Apis {
		Roles[g.Role].Assign(gorbac.NewStdPermission(val.URI))
	}
	Rbac.Add(Roles[g.Role])

	return g, nil
}

func (g *Group) Read(p interface{}, id string) (interface{}, error) {
	utils.Log.Info("Start Group Read")

	dbsess := p.(Dbsess)

	utils.Log.Debug("querying API")
	err := db.UserGroup(dbsess.Sess).FindId(id).One(g)
	if err != nil {
		return nil, err
	}

	utils.Log.Info("End Group Read")
	return g, nil
}

//ReadWParam for Group
func (g *Group) ReadWParam(p interface{}) (interface{}, error) {

	limit := bson.M{}
	queryPart := bson.M{}

	/*create query params*/
	req, ok := p.(GroupReadReq)
	dbsess := req.Dbsess
	q := req.Req
	page := req.Page

	if !ok {
		return nil, ErrParsing
	}

	if len(q.ID) > 0 {
		queryPart["_id"] = bson.M{"$in": q.ID}
	}

	if q.Role != 0 {
		queryPart["role"] = q.Role
	}

	if q.Name != "" {
		queryPart["name"] = q.Name
	}

	if page < 1 {
		page = 1
	}

	//do query
	utils.Log.Debug("querying group")

	u := new([]Group)
	body, err := utils.Paginate(db.UserGroup(dbsess.Sess), queryPart, limit, u, page, maxGroupPerPage, []string{})
	if err != nil {
		return nil, err
	}

	utils.Log.Info("End Group ReadWParam")
	return body, nil
}

func (g *Group) Update(p interface{}, id string) error {
	updt := p.(GroupUpdateReq)
	dbsess := updt.Dbsess.Sess
	req := updt.Req

	set := bson.M{}
	q := bson.M{}

	/* with Role == ID then Role should not be changed
		if req.Role != 0 {
		set["role"] = req.Role
	}*/
	if req.Name != "" {
		set["name"] = req.Name
	}
	if len(req.Apis) != 0 {
		apis, err := populateAPIBasedOnID(req.Apis, dbsess)
		if err != nil {
			return err
		}
		set["apis"] = apis
	}

	if len(set) == 0 {
		return errors.New("invalid update")
	}

	q["$set"] = set

	if err := db.UserGroup(dbsess).UpdateId(id, q); err != nil {
		utils.Log.Error("Failed to update api, reason=", err)
		return err
	}

	return nil
}

func (g *Group) Delete(p interface{}, id string) (int, error) {
	utils.Log.Debug("Deleting Group")

	dbsess := p.(Dbsess)
	if err := db.UserGroup(dbsess.Sess).RemoveId(id); err != nil {
		if err == mgo.ErrNotFound {
			utils.Log.Error("Group Not found")
			return iris.StatusNotFound, err
		}
		utils.Log.Error("Failed to query DB")
		return iris.StatusInternalServerError, err
	}

	//remove from rbac
	//TODO:
	//- reload Rbac
	//- handled by other service
	Rbac.Remove(id)

	utils.Log.Debug("Group Deleted")
	return 0, nil
}
