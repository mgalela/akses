package access

import (
	"errors"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
)

//TokenCache represent token cache document
type TokenCache struct {
	ID      string `json:"id" bson:"_id"`
	Token   string `json:"token" bson:"token"`
	UserID  string `json:"userid" bson:"userid"`
	Expired time.Time
}

//TokenCreate create a token
func TokenCreate(session *mgo.Session, user *User) (string, error) {
	var tc TokenCache

	if err := db.TokenCaches(session).Find(bson.M{"userid": user.ID}).One(&tc); err == nil {
		// checks old token
		if !time.Now().After(tc.Expired) {
			return tc.Token, nil
		} else {
			tc.Expired = time.Now().UTC().AddDate(1, 0, 0)

			if errr := db.TokenCaches(session).Update(bson.M{"userid": user.ID}, bson.M{"$set": tc}); errr != nil {
				return "", errr
			}
			return tc.Token, nil
		}
	}

	tc = TokenCache{
		ID:      bson.NewObjectId().Hex(),
		Token:   user.Token,
		UserID:  user.ID,
		Expired: time.Now().AddDate(1, 0, 0),
	}

	if err := db.TokenCaches(session).Insert(tc); err != nil {
		return "", err
	}

	return tc.Token, nil
}

//TokenMap map token to username
func TokenMap(session *mgo.Session, token string) (*TokenCache, error) {
	var tc TokenCache

	if err := db.TokenCaches(session).Find(bson.M{"token": token}).Sort("-expired").One(&tc); err != nil {
		utils.Log.Info("cannot find token=", token)
		return nil, err
	}

	if time.Now().UTC().After(tc.Expired) {
		utils.Log.Info("Token Map : token expired, please do login")
		return nil, errors.New("Token Expired,Please Login")
	}
	return &tc, nil
}

//TokenDelete delete token
func TokenDelete(session *mgo.Session, tokenid string) error {
	return db.TokenCaches(session).RemoveId(tokenid)
}
