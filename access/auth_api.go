package access

import (
	"errors"
	"fmt"
	"time"

	"github.com/kataras/iris"
	//mailgun "github.com/mailgun/mailgun-go"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"

	"golang.org/x/crypto/bcrypt"
)

const (
	invalidLoginMsg = "Invalid login"
)

//LoginRequest represent login request
type LoginRequest struct {
	Orgid    string `json:"orgid"`
	Source   string `json:"source"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Token    string `json:"token"`
}

//LoginResponse represent login response
type LoginResponse struct {
	ID        string    `json:"id"`
	Source    string    `json:"source"`
	Token     string    `json:"token"`
	Username  string    `json:"username"`
	Email     string    `json:"email"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Name      string    `json:"name"`
	Superuser bool      `json:"superuser"`
	Confirmed int       `json:"confirmed"`
	Active    int       `json:"active"`
	Created   time.Time `json:"created"`
}

func passwordHashGen(plainPass string) (string, error) {
	b, err := bcrypt.GenerateFromPassword([]byte(plainPass), bcrypt.DefaultCost)
	return string(b), err
}

//Login handle user login
func Login(ctx iris.Context) {
	utils.Log.Info("Start Processing Login Request")
	var loginRequest LoginRequest
	cred := new(Cred)
	var socId string
	var err error

	session := ctx.Values().Get("mongosession").(*mgo.Session)
	user := new(User)

	utils.Log.Debug("Bind Login Request")
	if err := ctx.ReadForm(&loginRequest); err != nil {
		utils.Log.Error("Error when binding request, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	if loginRequest.Source == "local" || loginRequest.Source == "" {
		utils.Log.Debug("validate local registration")
		if loginRequest.Email == "" && loginRequest.Username == "" {
			utils.Log.Error("Error either username or email is required")
			RespErrWithMsg(ctx, iris.StatusBadRequest, "Error either username or email is required")
			return
		}

		utils.Log.Debug("Authorize user")
		user, err = authorizeUser(session, &loginRequest)
		if err != nil {
			utils.Log.Error("unauthorize user, error=", err)
			RespErrWithMsg(ctx, iris.StatusUnauthorized, "unauthorize user")
			return
		}

		cred.Username = user.Username
	} else {
		if _, ok := socialAccountMap[loginRequest.Source]; !ok {
			RespErrWithMsg(ctx, iris.StatusUnauthorized, "unsupported source value")
			return
		}

		if loginRequest.Token == "" {
			RespErrWithMsg(ctx, iris.StatusUnauthorized, "missing token value")
			return
		}
		if loginRequest.Source == "facebook" {
			fbUser, err := getFacebookUser(loginRequest.Token)
			if err != nil {
				utils.Log.Error("failed to get Facebook account=", err)
				RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
				return
			}
			socId = fbUser.ID
		}

		if err := db.Users(session).Find(bson.M{"cred.source": loginRequest.Source, "cred.id": socId}).One(&user); err != nil {
			utils.Log.Error("unauthorize user, error=", err)
			RespErrWithMsg(ctx, iris.StatusUnauthorized, "unauthorize user")
			return
		}
		for _, v := range user.Cred {
			if v.Source == loginRequest.Source {
				cred.Username = v.Username
				cred.Name = v.Name
				cred.Confirmed = v.Confirmed
			}
		}
	}

	utils.Log.Debug("put token into tokencache")
	_, err = TokenCreate(session, user)
	if err != nil {
		utils.Log.Debug("Invalid Login")
		RespErrWithMsg(ctx, iris.StatusInternalServerError, invalidLoginMsg)
		return
	}

	loginResponse := LoginResponse{
		ID:        user.ID,
		Source:    loginRequest.Source,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Name:      cred.Name,
		Username:  cred.Username,
		Email:     user.Email,
		Confirmed: cred.Confirmed,
		Active:    user.Active,
		Created:   user.Created,
	}

	utils.Log.Info("update user's LastLogin")
	if err := db.Users(session).UpdateId(user.ID, bson.M{"$set": bson.M{"last_login": time.Now().UTC()}}); err != nil {
		if err != mgo.ErrNotFound {
			utils.Log.Error("Failed to query DB")
			RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
			return
		}
	}

	//c.JSON(http.StatusOK, gin.H{"msg": "OK", "data": loginResponse})
	RespOkWithData(ctx, iris.StatusOK, "OK", loginResponse)
}

func authorizeUser(session *mgo.Session, loginRequest *LoginRequest) (*User, error) {

	//utils.Log.Debug("org_id: ", orgid)

	user := User{}
	if loginRequest.Email != "" {
		utils.Log.Debug("get user by email")
		if err := db.Users(session).Find(bson.M{"email": loginRequest.Email}).One(&user); err != nil {
			if err == mgo.ErrNotFound {
				return nil, errors.New("user not found")
			}
			return nil, err
		}
	} else if loginRequest.Username != "" {
		utils.Log.Debug("get user by username")
		if err := db.Users(session).Find(bson.M{"username": loginRequest.Username}).One(&user); err != nil {
			if err == mgo.ErrNotFound {
				return nil, errors.New("user not found")
			}
			return nil, err
		}
	}

	// check if password equals to saved password
	utils.Log.Debug("check password")
	if err := passwordHashCheck(user.Password, loginRequest.Password); err != nil {
		return nil, fmt.Errorf("invalid password")
	}

	utils.Log.Debug("Check is user active")
	if user.Active != 1 {
		return nil, errors.New("user is not active")
	}

	return &user, nil
}

func passwordHashCheck(hashedPass, plainPass string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPass), []byte(plainPass))
}

//Logout logging out user from a system
func Logout(ctx iris.Context) {
	session := ctx.Values().Get("mongosession").(*mgo.Session)
	tokenid := ctx.Values().Get("tokenid").(string)

	//utils.Log.Info("logout.userid=", user.ID)
	if err := TokenDelete(session, tokenid); err != nil {
		utils.Log.Errorf("token deletion: %v", err.Error())
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	RespOk(ctx, iris.StatusOK)
}
