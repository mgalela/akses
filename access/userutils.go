package access

import (
	"context"
	"errors"
	"io"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/dchest/uniuri"
	"github.com/kataras/iris"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
)

type ctxKeyType string

const (
	prefixImgPath            = "/var/www/akses/upload"
	yearMonth                = "201601"
	dateprefix               = "20160102-150405"
	DateFmt                  = "02-01-2006"
	TimeFmt                  = "15:04"
	userCtxKey    ctxKeyType = "user"
	roleCtxKey    ctxKeyType = "role"
)

//ResetPasswordRequest represent reset password request
type ResetPasswordRequest struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

//ChangePasswordRequest represet change password req
type ChangePasswordRequest struct {
	Password    string `json:"password" binding:"required"`
	NewPassword string `json:"newpassword" binding:"required"`
}

type ForgotPasswdRequest struct {
	Email string `json:"email" binding:"required"`
}

type CheckEmailUsernameReq struct {
	Email    string `json:"email"`
	Username string `json:"username"`
}

type PushIdReq struct {
	PushId string `json:"pushid" binding:"required"`
}

func SetUser(ctx context.Context, user string) context.Context {
	return context.WithValue(ctx, userCtxKey, user)
}

func SetRole(ctx context.Context, role int) context.Context {
	return context.WithValue(ctx, roleCtxKey, role)
}

func GetUser(ctx context.Context) string {
	user, ok := ctx.Value(userCtxKey).(string)
	if !ok {
		// Log this issue
		return ""
	}
	return user
}

func GetRole(ctx context.Context) int {
	role, ok := ctx.Value(roleCtxKey).(int)
	if !ok {
		// Log this issue
		return 0
	}
	return role
}

//TODO: need to be optimized
func isUserExist(session *mgo.Session, username, email string) error {
	var err error

	user := new(User)
	if username != "" {
		utils.Log.Debug("verify if username exist")
		err = db.Users(session).Find(bson.M{"username": username}).One(user)
		if err == nil {
			return errors.New("username had been taken")
		}
	}

	utils.Log.Debug("verify if email exist")
	if email != "" {
		err = db.Users(session).Find(bson.M{"email": email}).One(user)
		if err == nil {
			return errors.New("email had been taken")
		}
	}

	return nil
}

//TODO: need to be optimized
func isOtherUserExist(session *mgo.Session, username, email, id string) error {
	var err error

	user := new(User)
	if username != "" {
		utils.Log.Debug("verify if other username exist")
		err = db.Users(session).Find(bson.M{"username": username}).One(user)
		if err == nil && user.ID != id {
			return errors.New("username had been taken")
		}
	}

	utils.Log.Debug("verify if other email exist")
	if email != "" {
		err = db.Users(session).Find(bson.M{"email": email}).One(user)
		if err == nil && user.ID != id {
			return errors.New("email had been taken")
		}
	}

	return nil
}

func validateRegistration(username, email, password string) error {
	rUsername := regexp.MustCompile("^[a-z0-9_\\.-]*$")
	rEmail := regexp.MustCompile("^([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$")

	if username != "" {
		if len(username) < 6 || len(username) > 40 {
			return errors.New("username length must be between 6 and 40 characters")
		}

		if !rUsername.MatchString(username) {
			return errors.New("username format invalid")
		}
	}

	if email != "" {
		if !rEmail.MatchString(email) {
			return errors.New("email format invalid")
		}
	}

	if password != "" {
		if len(password) < 8 {
			return errors.New("password must be more than 7 characters")
		}
	}

	return nil
}

func CheckEmailUsername(ctx iris.Context) {
	utils.Log.Info("Start CheckEmailUsername")
	var req CheckEmailUsernameReq
	session := ctx.Values().Get("mongosession").(*mgo.Session)

	utils.Log.Debug("Bind Request")
	if err := ctx.ReadForm(&req); err != nil {
		utils.Log.Error("Error when binding request, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	if req.Email != "" {
		n, err := db.Users(session).Find(bson.M{"email": req.Email}).Count()
		if err != nil {
			utils.Log.Error("Error when query, reason = ", err)
			RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
			return
		}
		if n > 0 {
			utils.Log.Error("email address already taken")
			RespErrWithMsg(ctx, iris.StatusInternalServerError, "email address already taken")
			return
		}
	}

	if req.Username != "" {
		n, err := db.Users(session).Find(bson.M{"username": req.Username}).Count()
		if err != nil {
			utils.Log.Error("Error when query, reason = ", err)
			RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
			return
		}
		if n > 0 {
			utils.Log.Error("username already taken")
			RespErrWithMsg(ctx, iris.StatusInternalServerError, "username already taken")
			return
		}
	}

	RespOk(ctx, iris.StatusOK)
}

func verifyEmail(session *mgo.Session, email string) error {
	rEmail := regexp.MustCompile("^([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$")

	if !rEmail.MatchString(email) {
		return errors.New("email format invalid")
	}

	user := User{}
	if err := db.Users(session).Find(bson.M{"email": email}).One(&user); err == nil {
		return errors.New("Sorry! This email has already been registered")
	}

	return nil
}

//UserByUsername get user by username
func UserByUsername(session *mgo.Session, username string) (*User, error) {
	user := User{}
	if err := db.Users(session).Find(bson.M{"username": username}).One(&user); err != nil {
		if err == mgo.ErrNotFound {
			return nil, errors.New("user not found")
		}
		return nil, err
	}
	return &user, nil
}

//UserByID get user by ID
func UserByID(session *mgo.Session, ID string, limit bson.M) (*User, error) {
	user := User{}
	if err := db.Users(session).FindId(ID).Select(limit).One(&user); err != nil {
		if err == mgo.ErrNotFound {
			return nil, errors.New("user not found")
		}
		return nil, err
	}
	return &user, nil
}

//UserByUsername get user by email
func UserByEmail(session *mgo.Session, email string) (*User, error) {
	user := User{}
	if err := db.Users(session).Find(bson.M{"email": email}).One(&user); err != nil {
		if err == mgo.ErrNotFound {
			return nil, errors.New("user not found")
		}
		return nil, err
	}
	return &user, nil
}

func Confirmation(ctx iris.Context) {
	utils.Log.Info("email confirmation")
	var key string

	session := ctx.Values().Get("mongosession").(*mgo.Session)

	//get param
	utils.Log.Debug("Build Parameter")

	if key = ctx.Params().Get("key"); key == "" {
		RespErrWithMsg(ctx, iris.StatusBadRequest, "key is required")
		return
	}

	keyCache, err := ConfirmKeyMap(session, key)
	if err != nil {
		utils.Log.Error("failed mapping key=", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	//Transisi: username ke userid
	if keyCache.UserID != "" {
		if err := db.Users(session).UpdateId(keyCache.UserID, bson.M{"$set": bson.M{"isconfirmed": true, "confirm_key": ""}}); err != nil {
			utils.Log.Error("failed to update user=", err)
			RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
			return
		}
	} else {
		if err := db.Users(session).Update(bson.M{"username": keyCache.Username}, bson.M{"$set": bson.M{"isconfirmed": true, "confirm_key": ""}}); err != nil {
			utils.Log.Error("failed to update user=", err)
			RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
			return
		}
	}

	ConfirmKeyDelete(session, keyCache.UserID)
	RespOk(ctx, iris.StatusOK)
}

//ResetPassword handle reset password
func ResetPassword(ctx iris.Context) {
	var resetPasswordReq ResetPasswordRequest

	session := ctx.Values().Get("mongosession").(*mgo.Session)

	if err := ctx.ReadForm(&resetPasswordReq); err != nil {
		utils.Log.Error("Error when binding request, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	pwd, err := passwordHashGen(resetPasswordReq.Password)

	if err != nil {
		utils.Log.Error("failed to generate hashed password=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	if err := db.Users(session).Update(bson.M{"username": resetPasswordReq.Username}, bson.M{"$set": bson.M{"password": pwd}}); err != nil {
		utils.Log.Error("failed to reset password=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	RespOk(ctx, iris.StatusOK)
}

//ChangePassword handle change password
func ChangePassword(ctx iris.Context) {
	utils.Log.Info("Start Change password")

	session := ctx.Values().Get("mongosession").(*mgo.Session)
	user := ctx.Values().Get("user").(*User)
	var changePasswordReq ChangePasswordRequest

	if err := ctx.ReadForm(&changePasswordReq); err != nil {
		utils.Log.Error("invalid format")
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	// check if password equals to saved password
	utils.Log.Debug("check current password")
	if err := passwordHashCheck(user.Password, changePasswordReq.Password); err != nil {
		RespErrWithMsg(ctx, iris.StatusBadRequest, "invalid current password")
		return
	}

	pwd, err := passwordHashGen(changePasswordReq.NewPassword)
	if err != nil {
		utils.Log.Error("failed to generate hashed password=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Debug("Processing Change password")
	if err := db.Users(session).Update(bson.M{"_id": user.ID}, bson.M{"$set": bson.M{"password": pwd}}); err != nil {
		utils.Log.Error("failed to update password=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End change password")
	RespOk(ctx, iris.StatusOK)
}

func ForgotPasswd(ctx iris.Context) {
	utils.Log.Info("Forgot Password")
	var req ForgotPasswdRequest

	session := ctx.Values().Get("mongosession").(*mgo.Session)
	orgid := ctx.Values().Get("orgID").(string)

	if err := ctx.ReadForm(&req); err != nil {
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	user := User{}
	if err := db.Users(session).Find(bson.M{"email": req.Email, "orgid": orgid}).One(&user); err != nil {
		if err == mgo.ErrNotFound {
			RespErrWithMsg(ctx, iris.StatusNotFound, err.Error())
			return
		}
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	_, err := KeyCreate(session, user.ID)
	if err != nil {
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	//	go sendForgotPasswdEmail(user.Email, key, org.Name)

	RespOkWithData(ctx, iris.StatusOK, "Forgot Password confirmation email has been sent to you.", "")
}

func NewPasswd(ctx iris.Context) {
	utils.Log.Info("request new password")
	var key string

	session := ctx.Values().Get("mongosession").(*mgo.Session)

	//get param
	utils.Log.Debug("Build Parameter")

	if key = ctx.Params().Get("key"); key == "" {
		RespErrWithMsg(ctx, iris.StatusBadRequest, "key is required")
		return
	}

	user_id, err := KeyMap(session, key)
	if err != nil {
		utils.Log.Error("failed mapping key=", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	user, err := UserByID(session, user_id, bson.M{})
	if err != nil {
		utils.Log.Error("failed to query user")
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	random_string := uniuri.NewLen(10)

	pwd, err := passwordHashGen(random_string)
	if err != nil {
		utils.Log.Error("failed to generate hashed password=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}
	if err := db.Users(session).UpdateId(user.ID, bson.M{"$set": bson.M{"password": pwd}}); err != nil {
		utils.Log.Error("failed to reset password=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	//	go sendNewPasswdEmail(user.Email, random_string, "")

	KeyDelete(session, user.ID)

	RespOkWithData(ctx, iris.StatusOK, "new password has been sent to your e-mail", "")
}

/*
func sendConfirmationEmail(recipientEmail, key, orgName string) {
	utils.Log.Info("sending confirmation email")
	activationBaseURI := getActivationURL()
	gun := mailgun.NewMailgun(Config.MailgunDomain, Config.MailgunPrivateKey, Config.MailgunPublicKey)

	subject := "Confirmation Request"
	recipient := []string{recipientEmail}
	body := fmt.Sprintf(`Thank you for signing up.
Please confirm your email address by following this link:

%s

Best Regards,
%s`, activationBaseURI+"?key="+key, orgName)
	m := mailgun.NewMessage(orgName+"<no-reply@qeponsquare.com>", subject, body, recipient...)
	_, _, err := gun.Send(m)
	if err != nil {
		utils.Log.Error("Error when sending email via mailgun, err = ", err)
	}
}

func sendForgotPasswdEmail(recipientEmail, key, orgName string) {
	utils.Log.Info("sending reset email")
	forgotPasswdBaseURI := getForgotPasswdURL()
	gun := mailgun.NewMailgun(Config.MailgunDomain, Config.MailgunPrivateKey, Config.MailgunPublicKey)

	subject := "Forgot Password Confirmation"
	recipient := []string{recipientEmail}
	body := fmt.Sprintf(`You can reset your password by following this link:

%s

Best Regards,
%s`, forgotPasswdBaseURI+"?key="+key, orgName)
	m := mailgun.NewMessage(orgName+"<no-reply@qeponsquare.com>", subject, body, recipient...)
	_, _, err := gun.Send(m)
	if err != nil {
		utils.Log.Error("Error when sending email via mailgun, err = ", err)
	}
}

func sendNewPasswdEmail(recipientEmail, pwd, orgName string) {
	utils.Log.Info("sending new password email")
	gun := mailgun.NewMailgun(Config.MailgunDomain, Config.MailgunPrivateKey, Config.MailgunPublicKey)

	subject := "Reset Password Notification"
	recipient := []string{recipientEmail}
	body := fmt.Sprintf(`Your password has been reset to:

%s

Change your password immediately.

Best Regards,
%s`, pwd, orgName)
	m := mailgun.NewMessage(orgName+"<no-reply@qeponsquare.com>", subject, body, recipient...)
	_, _, err := gun.Send(m)
	if err != nil {
		utils.Log.Error("Error when sending email via mailgun, err = ", err)
	}
}
*/

func getActivationURL() string {
	var url string
	//url = Config.ConfirmBaseUri
	if url == "" {
		url = "http://localhost:9013/api/confirmation"
	}
	return url
}

func getForgotPasswdURL() string {
	var url string
	//url = Config.ForgotPasswdBaseUri
	if url == "" {
		url = "http://localhost:9013/api/newpasswd"
	}
	return url
}

func UploadProfileImage(ctx iris.Context) {
	utils.Log.Info("Start upload image entry")
	//userid := c.MustGet("userid").(string)
	or_id := ctx.Values().Get("organization").(string)

	//read the form
	utils.Log.Info("read file from form")
	file, handler, err := ctx.FormFile("file")
	//log.Println("file, handler, err:", file, handler, err)
	if err != nil {
		utils.Log.Errorf("error when trying to get file, reason=%v", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}
	defer file.Close()

	now := time.Now()
	subdir := now.Format(yearMonth)
	userpath := "/" + or_id + "/profile/" + subdir
	userdir := prefixImgPath + userpath
	if _, err := os.Stat(userdir); err != nil {
		if os.IsNotExist(err) {
			err := os.MkdirAll(userdir, 0755)
			if err != nil {
				utils.Log.Errorf("error when create image dir, reason=%v", err)
				RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
				return
			}
		} else {
			utils.Log.Errorf("error when check image dir, reason=%v", err)
			RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
			return
		}
	}
	fileprefix := now.UTC().Format(dateprefix)
	imgpath := userpath + "/" + fileprefix + "_" + handler.Filename
	fileName := prefixImgPath + imgpath
	out, err := os.Create(fileName)
	defer out.Close()

	//write to storage
	utils.Log.Infof("writing file to %v", fileName)
	if _, err := io.Copy(out, file); err != nil {
		utils.Log.Errorf("error when writing file, reason=%v", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("end upload entry image")
	//go deleteUploadedFile(fileName)

	RespOkWithData(ctx, iris.StatusOK, "OK", imgpath)
}

func isContainString(str string, list []string) bool {
	for _, v := range list {
		if strings.Contains(str, v) {
			return true
		}
	}
	return false
}

func PushId(ctx iris.Context) {
	utils.Log.Info("Start Update PushId")

	session := ctx.Values().Get("mongosession").(*mgo.Session)
	user := ctx.Values().Get("user").(*User)
	var req PushIdReq
	source := "gcm"

	if err := ctx.ReadForm(&req); err != nil {
		utils.Log.Error("invalid format")
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	query := bson.M{}

	source = ctx.Params().Get("source")

	if source == "apn" {
		if !isContainString(req.PushId, user.Apn) {
			user.Apn = append(user.Apn, req.PushId)
			query["apnid"] = user.Apn
		}
	} else {
		if !isContainString(req.PushId, user.Gcm) {
			user.Gcm = append(user.Gcm, req.PushId)
			query["gcmid"] = user.Gcm
		}
	}

	if err := db.Users(session).UpdateId(user.ID, bson.M{"$set": query}); err != nil {
		utils.Log.Error("failed to update pushid=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End Update PushId")
	RespOk(ctx, iris.StatusOK)
}

/*
func sendConfirmationEmail(recipientEmail, key, orgName string) {
	utils.Log.Info("sending confirmation email")
	activationBaseURI := getActivationURL()
	gun := mailgun.NewMailgun(Config.MailgunDomain, Config.MailgunPrivateKey, Config.MailgunPublicKey)

	subject := "Confirmation Request"
	recipient := []string{recipientEmail}
	body := fmt.Sprintf(`Thank you for signing up.
Please confirm your email address by following this link:

%s

Best Regards,
%s`, activationBaseURI+"?key="+key, orgName)
	m := mailgun.NewMessage(orgName+"<no-reply@qeponsquare.com>", subject, body, recipient...)
	_, _, err := gun.Send(m)
	if err != nil {
		utils.Log.Error("Error when sending email via mailgun, err = ", err)
	}
}

func sendForgotPasswdEmail(recipientEmail, key, orgName string) {
	utils.Log.Info("sending reset email")
	forgotPasswdBaseURI := getForgotPasswdURL()
	gun := mailgun.NewMailgun(Config.MailgunDomain, Config.MailgunPrivateKey, Config.MailgunPublicKey)

	subject := "Forgot Password Confirmation"
	recipient := []string{recipientEmail}
	body := fmt.Sprintf(`You can reset your password by following this link:

%s

Best Regards,
%s`, forgotPasswdBaseURI+"?key="+key, orgName)
	m := mailgun.NewMessage(orgName+"<no-reply@qeponsquare.com>", subject, body, recipient...)
	_, _, err := gun.Send(m)
	if err != nil {
		utils.Log.Error("Error when sending email via mailgun, err = ", err)
	}
}

func sendNewPasswdEmail(recipientEmail, pwd, orgName string) {
	utils.Log.Info("sending new password email")
	gun := mailgun.NewMailgun(Config.MailgunDomain, Config.MailgunPrivateKey, Config.MailgunPublicKey)

	subject := "Reset Password Notification"
	recipient := []string{recipientEmail}
	body := fmt.Sprintf(`Your password has been reset to:

%s

Change your password immediately.

Best Regards,
%s`, pwd, orgName)
	m := mailgun.NewMessage(orgName+"<no-reply@qeponsquare.com>", subject, body, recipient...)
	_, _, err := gun.Send(m)
	if err != nil {
		utils.Log.Error("Error when sending email via mailgun, err = ", err)
	}
}
*/
