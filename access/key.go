package access

import (
	"errors"
	"time"

	"github.com/nu7hatch/gouuid"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
)

type KeyCache struct {
	Key     string `json:"key" bson:"key"`
	UserID  string `json:"user_id" bson:"user_id"`
	Expired time.Time
}

type ConfirmKeyCache struct {
	Key      string `json:"key" bson:"key"`
	UserID   string `json:"user_id" bson:"user_id"`
	Username string `json:"username" bson:"username"`
	Expired  time.Time
}

//KeyCreate create a key
func KeyCreate(session *mgo.Session, userid string) (string, error) {
	var k KeyCache
	u4, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	// creates new key
	k = KeyCache{
		Key:     u4.String(),
		UserID:  userid,
		Expired: time.Now().Add(1 * time.Hour),
	}

	if err := db.KeyCaches(session).Insert(k); err != nil {
		return "", err
	}

	return k.Key, nil
}

//KeyMap map key to username
func KeyMap(session *mgo.Session, key string) (string, error) {
	var k KeyCache

	if err := db.KeyCaches(session).Find(bson.M{"key": key}).One(&k); err != nil {
		utils.Log.Info("cannot find key=", key)
		return "", err
	}
	utils.Log.Debug("KeyMap key: ", k)

	if time.Now().UTC().After(k.Expired) {
		utils.Log.Info("Key Map : key expired, please send new request")
		return "", errors.New("Key expired, please send new request")
	}
	return k.UserID, nil
}

//TokenDelete delete token
func KeyDelete(session *mgo.Session, user_id string) error {
	return db.KeyCaches(session).Remove(bson.M{"user_id": user_id})
}

func ConfirmKeyCreate(session *mgo.Session, userid string) (string, error) {
	var k ConfirmKeyCache
	u4, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	// creates new key
	k = ConfirmKeyCache{
		Key:    u4.String(),
		UserID: userid,
		//		Expired:  time.Now().Add(1 * time.Hour),
	}

	if err := db.ConfirmKeyCaches(session).Insert(k); err != nil {
		return "", err
	}

	return k.Key, nil
}

//KeyMap map key to username
func ConfirmKeyMap(session *mgo.Session, key string) (*ConfirmKeyCache, error) {
	var k ConfirmKeyCache

	if err := db.ConfirmKeyCaches(session).Find(bson.M{"key": key}).One(&k); err != nil {
		utils.Log.Info("cannot find key=", key)
		return nil, err
	}
	utils.Log.Debug("ConfirmKeyMap key: ", k)

	/*if time.Now().UTC().After(k.Expired) {
		utils.Log.Info("Key Map : key expired, please send new request")
		return "", errors.New("Key expired, please send new request")
	}*/
	return &k, nil
}

//TokenDelete delete token
func ConfirmKeyDelete(session *mgo.Session, user_id string) error {
	return db.ConfirmKeyCaches(session).Remove(bson.M{"user_id": user_id})
}
