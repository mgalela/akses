package access

import (
	"strconv"

	"github.com/kataras/iris"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
)

//AddNewAPI handle create new api request
func ApiCreate(ctx iris.Context) {
	utils.Log.Info("Add API")
	session := ctx.Values().Get("mongosession").(*mgo.Session)
	var req ApiCreateParam

	if err := ctx.ReadForm(&req); err != nil {
		utils.Log.Error("Error when binding request, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	//create new API
	utils.Log.Debug("Create New API")
	var u Access
	p := ApiCreateRequest{
		Dbsess: Dbsess{
			Sess: session,
		},
		Req: req,
	}
	_, err := u.Create(p)

	if err != nil {
		utils.Log.Error("Error When Creating API, reason=", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End ApiCreate")
	RespOk(ctx, iris.StatusOK)
}

//GetAPI handle get api by parameter
func ApiRead(ctx iris.Context) {
	var page = 1
	var apiCode string
	var apiID string
	var err error
	maxPerPage := 50

	utils.Log.Info("Start Get API")
	session := ctx.Values().Get("mongosession").(*mgo.Session)
	//q := c.Request.URL.Query()

	utils.Log.Debug("Get query string parameter")
	//get api code
	apiCode = ctx.Params().Get("code")

	//get api id
	apiID = ctx.Params().Get("apiid")

	//get page
	if val := ctx.Params().Get("page"); val != "" {
		if page, err = strconv.Atoi(val); err != nil {
			utils.Log.Error(err.Error())
			RespErrWithMsg(ctx, iris.StatusBadRequest, "Failed parsing page")
			return
		}
	}

	utils.Log.Debug("Build query part")
	//build query
	queryPart := bson.M{}
	if apiCode != "" {
		queryPart["code"] = apiCode
	}

	if apiID != "" {
		queryPart["_id"] = apiID
	}

	utils.Log.Debug("do query get api")
	//do query
	apis := []API{}
	body, err := utils.Paginate(db.API(session), queryPart, bson.M{}, &apis, page, maxPerPage, []string{})
	if err != nil {
		utils.Log.Error(err.Error())
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End Get API")
	RespOkWithData(ctx, iris.StatusOK, "OK", body)
}

//UpdateAPI handle update api data
func ApiUpdate(ctx iris.Context) {
	var req ApiUpdateParam

	utils.Log.Info("Start Updating API Data")
	session := ctx.Values().Get("mongosession").(*mgo.Session)

	utils.Log.Debug("binding update api request")
	if err := ctx.ReadForm(&req); err != nil {
		utils.Log.Error("Error when binding request, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusBadRequest, err.Error())
		return
	}

	apiID := req.ID

	//validate api
	utils.Log.Debug("validate api exist")
	api := API{}
	if err := db.API(session).Find(bson.M{"_id": bson.M{"$ne": apiID}, "code": req.Code}).One(&api); err == nil {
		utils.Log.Debug("Duplicate API")
		RespErrWithMsg(ctx, iris.StatusBadRequest, "API with specified code already exist")
		return
	}

	//update api
	updtAPI := bson.M{
		"uri":  req.URI,
		"code": req.Code,
		"desc": req.Desc,
	}

	utils.Log.Debug("do update api")
	if err := db.API(session).UpdateId(apiID, bson.M{"$set": updtAPI}); err != nil {
		utils.Log.Error("Failed update api, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	//update api in group
	utils.Log.Debug("update api in group")
	if err := updateGroupByNewAPI(session, apiID, req.URI, req.Code); err != nil {
		utils.Log.Error("Failed update api in group, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	//repopulate access control
	utils.Log.Debug("repopulate access control")
	if err := updateAccessControlbyAPI(session, apiID); err != nil {
		utils.Log.Error("Failed repopulate access control, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End Updating API Data")
	RespOk(ctx, iris.StatusOK)
}

/*
//DeleteAPI handle delete API
func DeleteAPI(ctx iris.Context) {
	utils.Log.Info("Start Delete API By ID")
	session := ctx.Values().Get("mongosession").(*mgo.Session)

	var id string

	//get param
	utils.Log.Debug("get query string")

	if id = ctx.Params().Get("id"); id == "" {
		utils.Log.Error("id required")
		RespErrWithMsg(ctx, iris.StatusBadRequest, "id required")
		return
	}

	//do delete api
	utils.Log.Debug("Delete API ID : " + id)
	if err := db.API(session).RemoveId(id); err != nil {
		if err == mgo.ErrNotFound {
			utils.Log.Error("Not found")
			RespErrWithMsg(ctx, iris.StatusNotFound, "API not found")
			return
		}
		utils.Log.Error("Failed to query DB")
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	//find group with the api
	groups := []Group{}
	if err := db.UserGroup(session).Find(bson.M{"apis._id": id}).All(&groups); err != nil {
		utils.Log.Info("End Delete API")
		RespOk(ctx, iris.StatusOK)
		return
	}

	gid := []string{}
	for _, v := range groups {
		gid = append(gid, v.ID)
	}

	//do delete api on group
	utils.Log.Debug("Delete API on group")
	if _, err := db.UserGroup(session).UpdateAll(bson.M{"_id": bson.M{"$in": gid}}, bson.M{"$pull": bson.M{"apis._id": id}}); err != nil {
		if err != mgo.ErrNotFound {
			utils.Log.Error("Failed to query DB, reason=", err)
			RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
			return
		}
	}

	//repopulate access control
	utils.Log.Debug("repopulate access control")
	if err := updateAccessControlbyGID(session, gid); err != nil {
		utils.Log.Error("Failed repopulate access control, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End Delete API")
	RespOk(ctx, iris.StatusOK)
}*/

//ApiDelete Handle delete user
func ApiDelete(ctx iris.Context) {
	utils.Log.Info("Start Delete Api")
	session := ctx.Values().Get("mongosession").(*mgo.Session)

	var id string

	//get param
	utils.Log.Debug("get query string")

	if id = ctx.Params().Get("id"); id == "" {
		utils.Log.Error("id required")
		RespErrWithMsg(ctx, iris.StatusBadRequest, "id required")
		return
	}

	//do delete api
	var u Access
	rsp, err := u.Delete(session, id)
	if err != nil {
		RespErrWithMsg(ctx, rsp, err.Error())
		return
	}

	//find group with the api
	//TODO: handled by other service
	groups := []Group{}
	if err := db.UserGroup(session).Find(bson.M{"apis._id": id}).All(&groups); err != nil {
		utils.Log.Info("End Delete API")
		RespOk(ctx, iris.StatusOK)
		return
	}

	gid := []string{}
	for _, v := range groups {
		gid = append(gid, v.ID)
	}

	//do delete api on group
	utils.Log.Debug("Delete API on group")
	if _, err := db.UserGroup(session).UpdateAll(bson.M{"_id": bson.M{"$in": gid}}, bson.M{"$pull": bson.M{"apis._id": id}}); err != nil {
		if err != mgo.ErrNotFound {
			utils.Log.Error("Failed to query DB, reason=", err)
			RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
			return
		}
	}

	//repopulate access control
	//TODO: handled by other service
	utils.Log.Debug("repopulate access control")
	if err := updateAccessControlbyGID(session, gid); err != nil {
		utils.Log.Error("Failed repopulate access control, reason = ", err)
		RespErrWithMsg(ctx, iris.StatusInternalServerError, err.Error())
		return
	}

	utils.Log.Info("End Delete Api")
	RespOk(ctx, iris.StatusOK)
}
