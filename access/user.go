package access

import (
	"errors"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/dchest/uniuri"
	"github.com/kataras/iris"

	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
)

const (
	maxUserPerPage = 50
	defaultPass    = "passworddefault"
)

var (
	ErrParsing      = errors.New("Error Parsing")
	ErrInvalidParam = errors.New("Invalid parameters")
)

//Cred untuk simpan credential akses dari server lain
type Cred struct {
	Source    string `json:"source" bson:"source"`
	ID        string `json:"id" bson:"_id"`
	Token     string `json:"token" bson:"token"`
	Name      string `json:"name" bson:"name"`
	Username  string `json:"username" bson:"username"`
	Confirmed int    `json:"confirmed,string" bson:"confirmed"`
}

//User represent user
type User struct {
	ID         string    `json:"id" bson:"_id"`
	Email      string    `json:"email" bson:"email"`
	Username   string    `json:"username" bson:"username"`
	Password   string    `json:"password" bson:"password"`
	Role       int       `json:"role,string" bson:"role"`
	Groupid    string    `json:"groupid" bson:"groupid"`
	FirstName  string    `json:"first_name" bson:"first_name"`
	LastName   string    `json:"last_name" bson:"last_name"`
	Gcm        []string  `json:"gcmid"`
	Apn        []string  `json:"apnid"`
	Cred       []Cred    `json:"cred" bson:"cred"`
	Token      string    `json:"token" bson:"token"`
	ConfirmKey string    `json:"confirmkey" bson:"confirmkey"`
	Confirmed  int       `json:"confirmed,string" bson:"confirmed"`
	Active     int       `json:"active,string" bson:"active"`
	RegBy      string    `json:"regby" bson:"regby"`
	Created    time.Time `json:"created" bson:"created"`
}

type UserCreateRequest struct {
	Source    string `json:"source"`
	Username  string `json:"username"`
	Email     string `json:"email" binding:"required"`
	Password  string `json:"password" binding:"required"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Role      int    `json:"role,string"`
	Gcm       string `json:"gcmid"`
	Apn       string `json:"apnid"`
	Active    int    `json:"active,string"`
	RegBy     string `json:"regby"`
}

type CreateUser struct {
	Dbsess Dbsess
	Req    UserCreateRequest
}

type UserReadParamReq struct {
	Dbsess Dbsess
	Req    UserReadParam
	Page   int
}

type UserReadParam struct {
	ID        []string
	Email     string
	Username  string
	Role      int
	Active    int
	Confirmed int
	Regby     string
}

type UserUpdateReq struct {
	ID        string `json:"id" binding:"required"`
	Email     string `json:"email"`
	Username  string `json:"username"`
	Role      int    `json:"role,string"`
	Groupid   string `json:"groupid"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Gcm       string `json:"gcmid"`
	Apn       string `json:"apnid"`
	Cred      Cred   `json:"cred"`
	Active    int    `json:"active,string"`
}

type UpdateUser struct {
	Dbsess Dbsess        `json:"-"`
	Req    UserUpdateReq `json:"req"`
}

//Create method for User
func (user *User) Create(p interface{}) (interface{}, error) {
	reg := p.(CreateUser)
	dbsess := reg.Dbsess.Sess
	req := reg.Req

	if req.Email == "" {
		utils.Log.Error("empty email")
		return nil, errors.New("empty email")
	}

	if req.Source == "local" || req.Source == "" {
		utils.Log.Debug("validate local registration")
		if err := validateRegistration(req.Username, req.Email, req.Password); err != nil {
			utils.Log.Error(err.Error())
			return nil, err
		}
	} //TODO: register pakai akun social media

	utils.Log.Debug("verify local registration")
	err := isUserExist(dbsess, req.Username, req.Email)
	if err != nil {
		utils.Log.Error(err.Error())
		return nil, err
	}

	/*Create New User Data*/
	//user := new(User)
	user.ID = bson.NewObjectId().Hex()
	user.Username = req.Username
	user.Email = req.Email

	if req.Source == "local" || req.Source == "" {
		password := defaultPass
		if req.Password != "" {
			password = req.Password
		}
		hashedPass, err := passwordHashGen(password)
		if err != nil {
			utils.Log.Error("Registration failed, reason=", err)
			return nil, err
		}
		user.Password = hashedPass
	} /* else { //TODO: login dari source selain local
		cred := Cred{}
		cred.Source = src
		cred.ID = d.ID
		cred.Token = d.Token
		cred.Name = d.Name
		cred.Username = d.Username
		cred.Confirmed = 1
		user.Cred = append(user.Cred, cred)
	}*/
	user.Role = 3 //TODO: verifikasi Role
	user.Confirmed = 0
	user.Active = 1
	if req.RegBy == "system" {
		user.Role = req.Role
		user.Active = req.Active
		user.Confirmed = 1
	}
	user.RegBy = req.RegBy

	user.ConfirmKey, _ = ConfirmKeyCreate(dbsess, user.ID)
	user.FirstName = req.FirstName
	user.LastName = req.LastName

	if req.Gcm != "" {
		user.Gcm = append(user.Gcm, req.Gcm)
	}
	if req.Apn != "" {
		user.Apn = append(user.Apn, req.Apn)
	}

	user.Token = uniuri.NewLen(30) //TODO
	user.Created = time.Now().UTC()

	//save
	utils.Log.Info("Creating user")
	if err := db.Users(dbsess).Insert(user); err != nil {
		return nil, err
	}
	utils.Log.Infof("End Create User")

	return user, nil
}

func (d *User) Read(p interface{}, id string) (interface{}, error) {
	utils.Log.Info("Start User Read")

	dbsess := p.(Dbsess)

	limit := bson.M{"password": 0, "token": 0}

	utils.Log.Debug("querying user")
	d, err := UserByID(dbsess.Sess, id, limit)
	if err != nil {
		return nil, err
	}

	utils.Log.Info("End User Read")
	return d, nil
}

//ReadWParam for User
func (d *User) ReadWParam(p interface{}) (interface{}, error) {

	Limit := bson.M{"password": 0, "token": 0}
	queryPart := bson.M{}

	/*create query params*/
	req, ok := p.(UserReadParamReq)
	dbsess := req.Dbsess
	q := req.Req
	page := req.Page

	if !ok {
		return nil, ErrParsing
	}

	if len(q.ID) > 0 {
		queryPart["_id"] = bson.M{"$in": q.ID}
	}

	if q.Email != "" {
		queryPart["email"] = q.Email
	}

	if q.Username != "" {
		queryPart["username"] = q.Username
	}

	if q.Role != 0 {
		queryPart["role"] = q.Role
	}

	if q.Active != 0 {
		queryPart["active"] = q.Active
	}

	if q.Confirmed != 0 {
		queryPart["confirmed"] = q.Confirmed
	}

	if q.Regby != "" {
		queryPart["regby"] = q.Regby
	}

	if page < 1 {
		page = 1
	}

	//do query
	utils.Log.Debug("querying users")

	u := new([]User)
	body, err := utils.Paginate(db.Users(dbsess.Sess), queryPart, Limit, u, page, maxUserPerPage, []string{})
	if err != nil {
		return nil, err
	}

	utils.Log.Info("End User ReadWParam")
	return body, nil
}

//Update method for User
func (d *User) Update(p interface{}, id string) error {
	var err error
	updt := p.(UpdateUser)
	session := updt.Dbsess.Sess
	req := updt.Req

	utils.Log.Debug("validate username, email")
	if err = validateRegistration(req.Username, req.Email, ""); err != nil {
		utils.Log.Error(err.Error())
		return err
	}

	utils.Log.Debug("verify username, email")
	err = isOtherUserExist(session, req.Username, req.Email, id)
	if err != nil {
		utils.Log.Error(err.Error())
		return err
	}

	/*build update data*/
	set := bson.M{}
	push := bson.M{}
	q := bson.M{}

	if req.Username != "" {
		set["username"] = req.Username
	}
	if req.Email != "" {
		set["email"] = req.Email
	}
	if req.Role != 0 {
		set["role"] = req.Role
	}
	if req.FirstName != "" {
		set["first_name"] = req.FirstName
	}
	if req.LastName != "" {
		set["last_name"] = req.LastName
	}
	if req.Active != 0 {
		set["active"] = req.Active
	}
	if set != nil {
		q["$set"] = set
	}

	if req.Gcm != "" {
		push["gcm"] = req.Gcm
	}
	if req.Apn != "" {
		push["apn"] = req.Apn
	}
	if push != nil {
		q["$push"] = push
	}

	if err := db.Users(session).UpdateId(id, q); err != nil {
		utils.Log.Error("Failed to update user, reason=", err)
		return err
	}

	return nil
}

//Delete method for User
func (d *User) Delete(p interface{}, id string) (int, error) {
	utils.Log.Debug("Deleting User")

	dbsess := p.(Dbsess)
	if err := db.Users(dbsess.Sess).RemoveId(id); err != nil {
		if err == mgo.ErrNotFound {
			utils.Log.Error("User Not found")
			return iris.StatusNotFound, err
		}
		utils.Log.Error("Failed to query DB")
		return iris.StatusInternalServerError, err
	}

	utils.Log.Debug("User Deleted")
	return 0, nil
}
