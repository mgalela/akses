package access

import (
	"errors"

	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func populateAPIBasedOnID(r []string, session *mgo.Session) ([]API, error) {
	apis := []API{}
	for _, v := range r {
		api := API{}
		err := db.API(session).FindId(v).One(&api)
		if err != nil {
			return nil, errors.New("Failed to query DB, err = " + err.Error())
		}
		apis = append(apis, api)
	}
	return apis, nil
}

//GroupByName get group by its name
func GroupByName(session *mgo.Session, name string) (*Group, error) {
	var group Group
	if err := db.UserGroup(session).Find(bson.M{"name": name}).One(&group); err != nil {
		return nil, err
	}
	return &group, nil
}

func GroupById(session *mgo.Session, id string) (*Group, error) {
	var group Group
	if err := db.UserGroup(session).FindId(id).One(&group); err != nil {
		return nil, err
	}
	return &group, nil
}

func populateAPIBasedOnPutReq(r *GroupUpdateParam, session *mgo.Session) ([]API, error) {
	apis := []API{}
	for _, v := range r.Apis {
		api := API{}
		err := db.API(session).FindId(v).One(&api)
		if err != nil {
			return nil, errors.New("Failed to query DB, err = " + err.Error())
		}

		apis = append(apis, api)
	}
	return apis, nil
}

func updateUserByNewGroup(session *mgo.Session, groupID, groupName string, apis []API) error {
	if _, err := db.Users(session).UpdateAll(bson.M{"group._id": groupID}, bson.M{"$set": bson.M{"group.name": groupName, "group.apis": apis}}); err != nil {
		if err != mgo.ErrNotFound {
			utils.Log.Error("Failed to query db when update user, reason=", err)
			return errors.New("Failed to update existing group in api")
		}
	}

	return nil
}
