package access

import (
	"strconv"

	"github.com/mikespook/gorbac"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
)

var (
	//AccessControl is mapping role and access control
	Rbac  *gorbac.RBAC
	Roles map[int]*gorbac.StdRole
	//Perms map[int]gorbac.Permission
)

//InitAccessControl initialise access control
func InitAccessControl() error {
	Rbac = gorbac.New()
	return populateAccessControl(Rbac)
}

func populateAccessControl(rbac *gorbac.RBAC) error {
	Roles = make(map[int]*gorbac.StdRole)
	//Perms = make(map[int]gorbac.Permission)
	session, err := db.Session()

	defer session.Close()

	utils.Log.Info("Start populate access control")
	if err != nil {
		utils.Log.Error("failed to get DB session:", err)
		return err
	}

	utils.Log.Debug("get group from DB")
	groups := []Group{}
	if err := db.UserGroup(session).Find(bson.M{}).All(&groups); err != nil {
		if err != mgo.ErrNotFound {
			utils.Log.Error("Failed to query DB")
			return err
		}
	}

	//add group and uri to rbac
	utils.Log.Debug("Add group and uri to rbac")
	for _, group := range groups {
		Roles[group.Role] = gorbac.NewStdRole(strconv.Itoa(group.Role))
		for _, val := range group.Apis {
			//perm := gorbac.NewStdPermission(val.URI)
			Roles[group.Role].Assign(gorbac.NewStdPermission(val.URI))
		}
		Rbac.Add(Roles[group.Role])
	}

	utils.Log.Info("End populate access control")
	return nil
}
