package access

import (
	"errors"
	"strconv"

	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
	"github.com/mikespook/gorbac"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//TODO: optimized
func isCodeExist(dbsess *mgo.Session, code string) error {
	api := new(API)
	return db.API(dbsess).Find(bson.M{"code": code}).One(api)
}

func updateAccessControlbyAPI(session *mgo.Session, apiID string) error {
	groups := []Group{}
	if err := db.UserGroup(session).Find(bson.M{"apis._id": apiID}).All(&groups); err != nil {
		if err != mgo.ErrNotFound {
			utils.Log.Error("Failed to query db when find api group, error = ", err)
			return errors.New("Failed to find api in group")
		}
	}

	for _, group := range groups {
		Rbac.Remove(Roles[group.Role].ID())
		Roles[group.Role] = gorbac.NewStdRole(strconv.Itoa(group.Role))
		for _, val := range group.Apis {
			Roles[group.Role].Assign(gorbac.NewStdPermission(val.URI))
		}
		Rbac.Add(Roles[group.Role])

	}

	return nil
}

func updateAccessControlbyGID(session *mgo.Session, GID []string) error {
	groups := []Group{}
	if err := db.UserGroup(session).Find(bson.M{"_id": bson.M{"$in": GID}}).All(&groups); err != nil {
		if err != mgo.ErrNotFound {
			utils.Log.Error("Failed to query db when find api group, error = ", err)
			return errors.New("Failed to find api in group")
		}
	}

	for _, group := range groups {
		Rbac.Remove(Roles[group.Role].ID())
		Roles[group.Role] = gorbac.NewStdRole(strconv.Itoa(group.Role))
		for _, val := range group.Apis {
			Roles[group.Role].Assign(gorbac.NewStdPermission(val.URI))
		}
		Rbac.Add(Roles[group.Role])

	}

	return nil
}

func updateGroupByNewAPI(session *mgo.Session, apiID, newAPIURI, newAPICode string) error {
	if _, err := db.UserGroup(session).UpdateAll(bson.M{"apis._id": apiID}, bson.M{"$set": bson.M{"apis.api_code": newAPICode, "apis.api_uri": newAPIURI}}); err != nil {
		if err != mgo.ErrNotFound {
			utils.Log.Error("Failed to query db when update group,reason=", err)
			return errors.New("Failed to update existing api in group")
		}
	}

	return nil
}
