package main

import (
	"flag"
	"net/http"

	"github.com/mgalela/akses"
	"github.com/mgalela/akses/utils"
)

func main() {
	r := hard.BootstrapAPI()

	filename := flag.String("config", "config.json", "Path to configuration file")

	flag.Parse()

	hard.Initialize(*filename)

	utils.Log.Info("Starting app server at 9015")
	/*http.Handle("/", r)
	if err := http.ListenAndServe(":9015", nil); err != nil {
		utils.Log.Error("Starting app server failed, reason = ", err)
	}*/
	srv := &http.Server{Addr: ":9015", Handler: r}
	srv.ListenAndServe()
}
