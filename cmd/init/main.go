package main

import (
	"os"

	"github.com/mgalela/akses"
	"github.com/mgalela/akses/utils"
)

func main() {
	hard.BootstrapAPI()
	utils.Log.Info("Start super user & group")
	hard.Init1stUser()
	utils.Log.Info("Creating DefaultOrg")
	hard.InitDefaultOrg()
	utils.Log.Info("Registering api")
	hard.RunRegisterAPIList()
	utils.Log.Info("Done register api")
	utils.Log.Info("Initializing group")
	hard.InitGroup()
	utils.Log.Info("Done init group")
	os.Exit(1)
}
