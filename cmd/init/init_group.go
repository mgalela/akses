package hard

import (
	"strings"

	"github.com/mgalela/akses/access"
	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
)

const (
	defaultPass = "passworddefault"
)

var (
	startUser = access.RegisterRequest{
		Username:  "super",
		Password:  defaultPass,
		Role:      0,
		FirstName: "super",
		LastName:  "user",
		Email:     "super@user.com",
		RegBy:     "system",
	}
	startGroup = access.GroupRequest{
		Name: "super",
		Apis: []string{},
	}
	startOrg = access.CreateOrgRequest{
		Name: "Super",
		Code: "ffffff",
	}
	defaultOrg = access.CreateOrgRequest{
		Name: "Default",
		Code: "000000",
	}
	userApis = []string{
		"auth_user_get",
		"auth_user_me",
		"auth_user_update",
		"auth_user_changepassword",
		"auth_user_resetpassword",
		"auth_user_pushid",
		"org_get",
		"schedules_get",
		"forum_boks_add",
		"forum_boks_get",
		"forum_boks_update",
		"forum_boks_delete",
		"forum_boks_register",
		"forum_boks_unregister",
		"forum_boks_addmember",
		"forum_boks_activestate",
		"forum_boks_delmember",
		"forum_user_get",
		"forum_user_update",
		"forum_category_add",
		"forum_category_get",
		"forum_category_update",
		"forum_category_delete",
		"forum_category_subscribe",
		"forum_topic_add",
		"forum_topic_addpost",
		"forum_topic_get",
		"forum_topic_update",
		"forum_topic_delete",
		"forum_topic_subscribe",
		"forum_post_add",
		"forum_post_get",
		"forum_post_update",
		"forum_post_delete",
		"forum_imgupload"}
	oprApis = []string{
		"auth_user_get",
		"auth_user_me",
		"auth_user_update",
		"auth_user_changepassword",
		"auth_user_resetpassword",
		"auth_user_pushid",
		"org_get",
		"schedules_add",
		"schedules_get",
		"schedules_update",
		"schedules_delete",
		"schedules_imgupload",
		"schedules_batch",
		"forum_boks_add",
		"forum_boks_get",
		"forum_boks_update",
		"forum_boks_delete",
		"forum_boks_register",
		"forum_boks_unregister",
		"forum_boks_addmember",
		"forum_boks_activestate",
		"forum_boks_delmember",
		"forum_user_get",
		"forum_user_update",
		"forum_category_add",
		"forum_category_get",
		"forum_category_update",
		"forum_category_delete",
		"forum_category_subscribe",
		"forum_topic_add",
		"forum_topic_addpost",
		"forum_topic_get",
		"forum_topic_update",
		"forum_topic_delete",
		"forum_topic_subscribe",
		"forum_post_add",
		"forum_post_get",
		"forum_post_update",
		"forum_post_delete",
		"forum_imgupload"}
	adminApis = []string{
		"auth_group_get",
		"auth_user_get",
		"auth_user_me",
		"auth_user_update",
		"auth_user_delete",
		"auth_user_changepassword",
		"auth_user_resetpassword",
		"auth_user_pushid",
		"org_get",
		"org_update",
		"schedules_add",
		"schedules_get",
		"schedules_update",
		"schedules_delete",
		"schedules_imgupload",
		"schedules_batch",
		"forum_boks_add",
		"forum_boks_get",
		"forum_boks_update",
		"forum_boks_delete",
		"forum_boks_register",
		"forum_boks_unregister",
		"forum_boks_addmember",
		"forum_boks_activestate",
		"forum_boks_delmember",
		"forum_user_get",
		"forum_user_update",
		"forum_category_add",
		"forum_category_get",
		"forum_category_update",
		"forum_category_delete",
		"forum_category_subscribe",
		"forum_topic_add",
		"forum_topic_addpost",
		"forum_topic_get",
		"forum_topic_update",
		"forum_topic_delete",
		"forum_topic_subscribe",
		"forum_post_add",
		"forum_post_get",
		"forum_post_update",
		"forum_post_delete",
		"forum_imgupload"}
)

func Init1stUser() {
	session, _ := db.Session()
	defer session.Close()
	superUserReq := startUser
	superUserGroupReq := startGroup
	superUserOrg := startOrg

	//insert group
	group, _ := access.NewGroup(session, &superUserGroupReq)
	superUserReq.Groupid = group.ID
	superUserReq.Role = group.Name

	//insert org
	org, _ := access.NewOrg(session, &superUserOrg)
	superUserReq.Orgid = org.ID

	_, _, err := access.NewUser(session, &superUserReq, org)
	if err != nil {
		utils.Log.Error("NewUser failed, reason=", err)
		return
	}
}

func InitDefaultOrg() {
	session, _ := db.Session()
	defer session.Close()

	defaultOrg := defaultOrg

	//insert org
	_, err := access.NewOrg(session, &defaultOrg)
	if err != nil {
		utils.Log.Error("create default org failed, reason=", err)
		return
	}
}

func checkApi(str string, list []string) bool {
	for _, v := range list {
		if strings.Contains(str, v) {
			return true
		}
	}
	return false
}

func InitGroup() {
	session, _ := db.Session()
	defer session.Close()

	//get APIs
	apis := []access.AnodeAPI{}
	if err := db.AnodeAPI(session).Find(nil).All(&apis); err != nil {
		utils.Log.Error("Failed to get api list")
		return
	}

	userGroupReq := access.GroupRequest{
		Name: "user",
		Apis: []string{},
	}

	for _, v := range apis {
		if checkApi(v.Code, userApis) {
			userGroupReq.Apis = append(userGroupReq.Apis, v.ID)
		}
	}
	if _, err := access.NewGroup(session, &userGroupReq); err != nil {
		utils.Log.Error("Create Group user failed, reason=", err)
		return
	}

	oprGroupReq := access.GroupRequest{
		Name: "opr",
		Apis: []string{},
	}

	for _, v := range apis {
		if checkApi(v.Code, oprApis) {
			oprGroupReq.Apis = append(oprGroupReq.Apis, v.ID)
		}
	}

	if _, err := access.NewGroup(session, &oprGroupReq); err != nil {
		utils.Log.Error("Create Group opr failed, reason=", err)
		return
	}

	adminGroupReq := access.GroupRequest{
		Name: "admin",
		Apis: []string{},
	}

	for _, v := range apis {
		if checkApi(v.Code, adminApis) {
			adminGroupReq.Apis = append(adminGroupReq.Apis, v.ID)
		}
	}

	if _, err := access.NewGroup(session, &adminGroupReq); err != nil {
		utils.Log.Error("Create Group admin failed, reason=", err)
		return
	}
}
