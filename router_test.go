package hard

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gopkg.in/mgo.v2"

	simplejson "github.com/bitly/go-simplejson"
	"github.com/gin-gonic/gin"
	"github.com/mgalela/akses/access"
	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
)

var (
	apiEngine = BootstrapAPI()
)

type PaginationResp struct {
	Status    string           `json:"status"`
	StatusMsg string           `json:"statusmsg"`
	Data      utils.Pagination `json:"data"`
}

func testGetString(j *simplejson.Json, key string) string {
	return j.Get(key).MustString()
}

func testRequest(method, route, body string,
	header map[string]string, t *testing.T, eng *gin.Engine) (*httptest.ResponseRecorder, *simplejson.Json) {
	if eng == nil {
		eng = apiEngine
	}

	resp := httptest.NewRecorder()
	br := strings.NewReader(body)
	req, err := http.NewRequest(method, route, br)
	if err != nil {
		t.Fatalf("http request failed:%v", err)
	}

	for key, value := range header {
		req.Header.Set(key, value)
	}

	eng.ServeHTTP(resp, req)
	j, err := simplejson.NewFromReader(resp.Body)
	if err != nil {
		t.Fatalf("failed to decode json response:%v", err)
	}

	return resp, j
}

func buildAuthHeader(ses *mgo.Session, user *access.User, orgid string) map[string]string {
	// logged in as a user
	//lr := LoginRequest{Source: "local", Username: username, Password: password}

	token, _ := access.TokenCreate(ses, user, orgid)

	h := map[string]string{
		"Authorization": token,
		"Content-Type":  "application/json",
	}
	return h
}

func buildOrganizationAuth(ses *mgo.Session, organizationID string) map[string]string {
	org := access.Org{}
	db.Orgs(ses).FindId(organizationID).One(&org)

	h := map[string]string{
		"Authorization": org.Token,
		"Content-Type":  "application/json",
	}
	return h

}

func testSimpleRequest(method, route, body string,
	header map[string]string, engine *gin.Engine) (*httptest.ResponseRecorder, error) {

	resp := httptest.NewRecorder()
	br := strings.NewReader(body)
	req, err := http.NewRequest(method, route, br)
	if err != nil {
		return nil, err
	}

	for key, value := range header {
		req.Header.Set(key, value)
	}

	if engine == nil {
		engine = apiEngine
	}
	engine.ServeHTTP(resp, req)

	return resp, nil
}

func testRequestUnmarshal(method, route, body string,
	header map[string]string, eng *gin.Engine, v interface{}) (*httptest.ResponseRecorder, error) {

	resp, err := testSimpleRequest(method, route, body, header, eng)
	if err != nil {
		return nil, err
	}

	if err := json.NewDecoder(resp.Body).Decode(v); err != nil {
		return resp, err
	}
	return resp, nil
}
