package hard

import "github.com/kataras/iris"

type Resp struct {
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

var (
	RespOK = Resp{Msg: "OK"}
)

func RespErrWithMsg(ctx iris.Context, status int, msg string) {
	ctx.StatusCode(status)
	resp := Resp{Msg: msg}
	ctx.JSON(resp)
}

func RespOkWithData(ctx iris.Context, status int, msg string, data interface{}) {
	ctx.StatusCode(status)
	resp := Resp{Msg: msg, Data: data}
	ctx.JSON(resp)
}

func RespOk(ctx iris.Context, status int) {
	ctx.StatusCode(status)
	ctx.JSON(RespOK)
}
