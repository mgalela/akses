package hard

import (
	"errors"

	"github.com/kataras/iris"
	"github.com/mgalela/akses/access"
	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
	"github.com/mikespook/gorbac"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type ctxKeyType string

const (
	userCtxKey    = "user"
	roleCtxKey    = "role"
	tokenidCtxKey = "tokenid"
	dbCtxKey      = "mongosession"
)

func SetDbses(ctx iris.Context, dbses *mgo.Session) {
	ctx.Values().Set(dbCtxKey, dbses)
}

func SetUser(ctx iris.Context, user string) {
	ctx.Values().Set(userCtxKey, user)
}

func SetRole(ctx iris.Context, role int) {
	ctx.Values().Set(roleCtxKey, role)
}

func SetToken(ctx iris.Context, tokenid string) {
	ctx.Values().Set(tokenidCtxKey, tokenid)
}

func GetDBses(ctx iris.Context) *mgo.Session {
	dbses := ctx.Values().Get(dbCtxKey).(*mgo.Session)
	return dbses
}

func GetUser(ctx iris.Context) string {
	user := ctx.Values().Get(userCtxKey).(string)
	return user
}

func GetRole(ctx iris.Context) int {
	role := ctx.Values().Get(roleCtxKey).(int)
	return role
}

func GetToken(ctx iris.Context) string {
	tokenid := ctx.Values().Get(tokenidCtxKey).(string)
	return tokenid
}

//GetDB get mongo session
func GetDB(ctx iris.Context) {
	session, err := db.Session()
	if err != nil {
		utils.Log.Error("failed to get DB session:", err)
		//c.JSON(http.StatusInternalServerError, nil)
		ctx.StatusCode(iris.StatusInternalServerError)
		resp := Resp{Msg: err.Error()}
		ctx.JSON(resp)
	}
	//ctx.Values().Set("mongosession", session)
	SetDbses(ctx, session)
	ctx.Next()
	defer session.Close()
}

//CORS set header
/*func CORS() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "DELETE,PATCH,GET,POST,PUT")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(200)
		}
	}
}*/

func getTokenCache(ctx iris.Context, dbses *mgo.Session) (*access.TokenCache, error) {
	//capture token from http header
	token := ctx.GetHeader("Authorization")
	if len(token) == 0 {
		utils.Log.Info("null token")
		utils.AbortWithStatus(ctx, iris.StatusForbidden)
		return nil, errors.New("Null Token")
	}

	//check if token available in tokenCache
	tokenCache, err := access.TokenMap(dbses, token)
	if err != nil {
		utils.Log.Info("invalid token=", token)
		utils.AbortWithStatus(ctx, iris.StatusForbidden)
		return nil, errors.New("Invalid Token")
	}
	return tokenCache, nil
}

/*Auth handle authtoken and role
check authorization string in HTTP Header
*/
func Auth(ctx iris.Context) {
	ses := GetDBses(ctx)
	tokenCache, err := getTokenCache(ctx, ses)
	if err != nil {
		utils.AbortWithStatus(ctx, iris.StatusForbidden)
		return
	}

	//query user from database
	user := new(access.User)
	var errr error
	//validate role
	utils.Log.Debug("Middleware : Get User By ID = " + tokenCache.UserID)
	user, errr = access.UserByID(ses, tokenCache.UserID, bson.M{})
	if errr != nil {
		utils.Log.Info("Internal Error, err =", errr)
		utils.AbortWithStatus(ctx, iris.StatusInternalServerError)
		return
	}

	//set user & role value in context
	SetUser(ctx, user.ID)
	SetRole(ctx, user.Role)

	requestURL := ctx.Path()
	if !access.Rbac.IsGranted(access.Roles[user.Role].ID(), gorbac.NewStdPermission(requestURL), nil) {
		utils.Log.Infof("Unauthorized to access: %v", requestURL)
		utils.AbortWithMsg(ctx, iris.StatusForbidden, "Unauthorized Access Control")
		return
	}
}

/*Token handle token auth
check if token available in tokenCache and not expired
set tokenid value in context to be used by logout handler
*/
func Token(ctx iris.Context) {
	ses := GetDBses(ctx)
	tokenCache, err := getTokenCache(ctx, ses)
	if err != nil {
		utils.AbortWithStatus(ctx, iris.StatusForbidden)
		return
	}

	SetToken(ctx, tokenCache.ID)
	ctx.Next()
}
