package utils

import (
	"github.com/Sirupsen/logrus"
)

var (
	//Log initialise logger
	Log *logrus.Logger
)

func InitLog() error {
	var err error

	Log = logrus.New()
	Log.SetLevel(logrus.DebugLevel)
	//Log.Level = logrus.Level(logrus.DebugLevel)
	//hook := logrus_logentries.NewLogentriesHook(appName, logentriesToken)
	//Log.Hooks.Add(hook)
	return err
}
