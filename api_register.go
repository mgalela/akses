package hard

import (
	"sync"

	"gopkg.in/mgo.v2"

	"github.com/mgalela/akses/access"
	"github.com/mgalela/akses/db"
	"github.com/mgalela/akses/utils"
)

var (
	newAPIList = []access.APIRequest{
		{
			Code: "auth_api_add",
			URI:  "/auth/api/add",
		},
		{
			Code: "auth_api_get",
			URI:  "/api/v1/auth/api/get",
		},
		{
			Code: "auth_api_update",
			URI:  "/api/v1/auth/api/update",
		},
		{
			Code: "auth_api_delete",
			URI:  "/api/v1/auth/api/delete",
		},
		{
			Code: "auth_group_add",
			URI:  "/api/v1/auth/group/add",
		},
		{
			Code: "auth_group_get",
			URI:  "/api/v1/auth/group/get",
		},
		{
			Code: "auth_group_update",
			URI:  "/api/v1/auth/group/update",
		},
		{
			Code: "auth_group_delete",
			URI:  "/api/v1/auth/group/delete",
		},
		{
			Code: "auth_user_get",
			URI:  "/api/v1/auth/user/get",
		},
		{
			Code: "auth_user_me",
			URI:  "/api/v1/auth/user/me",
		},
		{
			Code: "auth_user_update",
			URI:  "/api/v1/auth/user/update",
		},
		{
			Code: "auth_user_delete",
			URI:  "/api/v1/auth/user/delete",
		},
		{
			Code: "auth_user_changepassword",
			URI:  "/api/v1/auth/user/changepassword",
		},
		{
			Code: "auth_user_resetpassword",
			URI:  "/api/v1/auth/user/resetpassword",
		},
		{
			Code: "auth_user_pushid",
			URI:  "/api/v1/auth/user/pushid",
		},
		{
			Code: "org_add",
			URI:  "/api/v1/org/add",
		},
		{
			Code: "org_get",
			URI:  "/api/v1/org/get",
		},
		{
			Code: "org_update",
			URI:  "/api/v1/org/update",
		},
		{
			Code: "org_delete",
			URI:  "/api/v1/org/delete",
		},
		/*		{
					Code: "profiles_add",
					URI:  "/api/v1/profiles/add",
				},
				{
					Code: "profiles_get",
					URI:  "/api/v1/profiles/get",
				},
				{
					Code: "profiles_update",
					URI:  "/api/v1/profiles/update",
				},
				{
					Code: "profiles_delete",
					URI:  "/api/v1/profiles/delete",
				},
				{
					Code: "profiles_imgupload",
					URI:  "/api/v1/profiles/imgupload",
				},
		*/{
			Code: "schedule_add",
			URI:  "/api/v1/schedule/add",
		},
		{
			Code: "schedule_batch",
			URI:  "/api/v1/schedule/batch",
		},
		{
			Code: "schedule_get",
			URI:  "/api/v1/schedule/get",
		},
		{
			Code: "schedule_update",
			URI:  "/api/v1/schedule/update",
		},
		{
			Code: "schedule_delete",
			URI:  "/api/v1/schedule/delete",
		},
		{
			Code: "schedule_imgupload",
			URI:  "/api/v1/schedule/imgupload",
		},
		{
			Code: "forum_boks_add",
			URI:  "/api/v1/forum/boks/add",
		},
		{
			Code: "forum_boks_get",
			URI:  "/api/v1/forum/boks/get",
		},
		{
			Code: "forum_boks_update",
			URI:  "/api/v1/forum/boks/update",
		},
		{
			Code: "forum_boks_delete",
			URI:  "/api/v1/forum/boks/delete",
		},
		{
			Code: "forum_boks_register",
			URI:  "/api/v1/forum/boks/register",
		},
		{
			Code: "forum_boks_unregister",
			URI:  "/api/v1/forum/boks/unregister",
		},
		{
			Code: "forum_boks_addmember",
			URI:  "/api/v1/forum/boks/addmember",
		},
		{
			Code: "forum_boks_activestate",
			URI:  "/api/v1/forum/boks/activestate",
		},
		{
			Code: "forum_boks_delmember",
			URI:  "/api/v1/forum/boks/delmember",
		},
		{
			Code: "forum_user_get",
			URI:  "/api/v1/forum/user/get",
		},
		{
			Code: "forum_user_update",
			URI:  "/api/v1/forum/user/update",
		},
		{
			Code: "forum_category_add",
			URI:  "/api/v1/forum/category/add",
		},
		{
			Code: "forum_category_get",
			URI:  "/api/v1/forum/category/get",
		},
		{
			Code: "forum_category_update",
			URI:  "/api/v1/forum/category/update",
		},
		{
			Code: "forum_category_delete",
			URI:  "/api/v1/forum/category/delete",
		},
		{
			Code: "forum_category_subscribe",
			URI:  "/api/v1/forum/category/subscribe",
		},
		{
			Code: "forum_topic_add",
			URI:  "/api/v1/forum/topic/add",
		},
		{
			Code: "forum_topic_addpost",
			URI:  "/api/v1/forum/topic/addpost",
		},
		{
			Code: "forum_topic_get",
			URI:  "/api/v1/forum/topic/get",
		},
		{
			Code: "forum_topic_update",
			URI:  "/api/v1/forum/topic/update",
		},
		{
			Code: "forum_topic_delete",
			URI:  "/api/v1/forum/topic/delete",
		},
		{
			Code: "forum_topic_subscribe",
			URI:  "/api/v1/forum/topic/subscribe",
		},
		{
			Code: "forum_post_add",
			URI:  "/api/v1/forum/post/add",
		},
		{
			Code: "forum_post_get",
			URI:  "/api/v1/forum/post/get",
		},
		{
			Code: "forum_post_update",
			URI:  "/api/v1/forum/post/update",
		},
		{
			Code: "forum_post_delete",
			URI:  "/api/v1/forum/post/delete",
		},
		{
			Code: "forum_imgupload",
			URI:  "/api/v1/forum/imgupload",
		},
	}
)

//RunRegisterAPIList is function to register new api
func RunRegisterAPIList() {
	var wg sync.WaitGroup
	session, _ := db.Session()
	defer session.Close()

	for _, api := range newAPIList {
		wg.Add(1)
		go func(sessionCopy *mgo.Session, apiReq access.APIRequest) {
			var u access.Access
			p := access.CreateApi{
				Dbsess: access.Dbsess{
					Sess: sessionCopy,
				},
				Req: apiReq,
			}
			_, err := u.Create(p)
			//newAPI, err := access.NewAPI(sessionCopy, &apiReq)
			if err != nil {
				utils.Log.Error("Error inserting new api with URI = ", err)
				return
			}
			defer sessionCopy.Close()
			defer wg.Done()
		}(session.Copy(), api)
	}
	wg.Wait()
}
