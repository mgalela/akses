package hard

import (
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"

	"github.com/mgalela/akses/access"
	"github.com/mgalela/akses/utils"
)

const (
	appName = "hard"
)

//BootstrapAPI run api
func BootstrapAPI() *iris.Application { //*gin.Engine {
	if err := utils.InitLog(); err != nil {
		utils.Log.Fatal("Initialisation failed, reason = ", err)
	}

	if err := access.InitAccessControl(); err != nil {
		utils.Log.Fatal("Access control initialisation failed, reason=", err)
	}

	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"}, // allows everything, use that to change the hosts.
		AllowCredentials: true,
	})

	r := iris.New()
	r.UseGlobal(crs, GetDB)

	r.Post("/api/login", access.Login)
	r.Get("/api/confirmation", access.Confirmation)
	r.Post("/api/forgotpasswd", access.ForgotPasswd)
	r.Get("/api/newpasswd", access.NewPasswd)
	r.Post("/api/logout", Token, access.Logout)
	r.Post("/api/register", access.UserCreate)
	r.Post("/api/checkemailusername", access.CheckEmailUsername)

	accessv1 := r.Party("/api/v1", Auth)
	accessv1.Post("/auth/api/add", access.ApiCreate)
	accessv1.Get("/auth/api/get", access.ApiRead)
	accessv1.Put("/auth/api/update", access.ApiUpdate)
	accessv1.Delete("/auth/api/delete", access.ApiDelete)

	accessv1.Post("/auth/group/add", access.GroupCreate)
	accessv1.Put("/auth/group/update", access.GroupUpdate)
	accessv1.Get("/auth/group/get", access.GroupRead)
	accessv1.Delete("/auth/group/delete", access.GroupDelete)

	accessv1.Post("/auth/user/update", access.UserUpdate)
	accessv1.Delete("/auth/user/delete", access.UserDelete)
	accessv1.Get("/auth/user/get", access.UserReadWParam)
	accessv1.Put("/auth/user/pushid", access.PushId)

	accessv1.Put("/auth/user/changepassword", access.ChangePassword)
	accessv1.Put("/auth/user/resetpassword", access.ResetPassword)

	return r
}

func test(ctx iris.Context) {
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(RespOK)
}
